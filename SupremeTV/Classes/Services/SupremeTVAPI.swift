//
//  SupremeTVAPI.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import Alamofire

class SupremeTVAPI: NSObject {

    // MARK: Show API

    class func getCategories(onSuccess: @escaping (_ categoryArray: [CategoryWithShowModel]) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.showMainDetails(completion: {(data: [CategoryWithShowModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [CategoryWithShowModel]())
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
    
    class func getHomeBanners(onSuccess: @escaping (_ bannersArray: [TvShowModel]) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.fetchAllBanners(page: "1", perpage: "10", completion: {(data: [TvShowModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [TvShowModel]())
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
    
    class func getSearchedProgrammes(withSearchKey: String, onSuccess: @escaping (_ programmes: [TvShowModel]) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.fetchShowBySearchKey(key: withSearchKey, completion: {(data: [TvShowModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [TvShowModel]())
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
    
    class func getTvShow(withId: String, onSuccess: @escaping (_ data: TvShowModel) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.fetchOneShowsByCategory(_id: withId, completion: {(data: TvShowModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? TvShowModel())
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
        
    class func getComments(withId: String, onSuccess: @escaping (_ comments: [ShowCommentModel]) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.getTvComments(_id: withId, completion: {(data: [ShowCommentModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [ShowCommentModel]())
            }else{
                onError("")
            }
        })
    }
    
    class func createComment(commentData: ShowCommentModel, onSuccess: @escaping (_ data: ShowCommentModel) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.createComment(commentData: commentData, completion: {(data: ShowCommentModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? ShowCommentModel())
            }else{
                onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
            }
        })
    }
    
    class func getTrendingshows(onSuccess: @escaping (_ comments: [TvShowModel]) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.fetchAllShowsByCategory(tranding:true, category: nil, subCategory: nil, page: nil, perpage: nil, completion: {(data: [TvShowModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [TvShowModel]())
            }else{
                onError("")
            }
        })
    }
    
    class func getScheduleProgrammes(withDate: String, onSuccess: @escaping (_ programmes: [ScheduleModel]) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.fetchAllSchedules(date: withDate, completion: {(data: [ScheduleModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [ScheduleModel]())
            }else{
                onError("")
            }
        })
    }
    
    class func getNowShowing(onSuccess: @escaping (_ nowShowing: ScheduleModel) -> Void, onError: @escaping (String) -> Void) {
        ShowsAPI.nowShowing(completion: {(data: ScheduleModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? ScheduleModel())
            }else{
                onError("")
            }
        })
    }
    
}

// MARK: Auth API

extension SupremeTVAPI{
    
    class func userLogin(loginData: User, onSuccess: @escaping (_ user: User) -> Void, onError: @escaping (String) -> Void) {
        AuthAPI.userLogin(loginData: loginData, completion: {(data: User?,_ error: Error?) -> Void in
            if error == nil {
                onSuccess(data ?? User())
            }else{
                let errorCode = error?.errorCode
                switch errorCode {
                case 404:
                    onError("Incorrect username or password")
                    break
                case 430:
                    onError("You are already logged in. Please sign out of your other device first.")
                    break
                case 401:
                    onError("Incorrect username or password")
                    break
                case 420:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                default:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                }
            }
        })
    }
    
   class func userForgotPassword(data: User, onSuccess: @escaping (_ data: CommonSuccess) -> Void, onError: @escaping (String) -> Void) {
        AuthAPI.userForgotPassword(data: data, completion: {(data: CommonSuccess?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? CommonSuccess())
            }else{
                onError("")
            }
        })
    }
            
    class func userRegister(registerData: User, onSuccess: @escaping (_ data: User) -> Void, onError: @escaping (String) -> Void) {
        AuthAPI.userRegister(registerData: registerData, completion: {(data: User?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? User())
            }else{
                let errorCode = error?.errorCode
                switch errorCode {
                case 410:
                    onError("Required feild missing")
                    break
                case 409:
                    onError("User already exists")
                    break
                default:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                }
            }
        })
    }
    
    class func userOauthLogin(loginData: OauthLogin, onSuccess: @escaping (_ data: User) -> Void, onError: @escaping (String) -> Void) {
        AuthAPI.userOauthLogin(loginData: loginData, completion: {(data: User?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? User())
            }else{
                let errorCode = error?.errorCode
                switch errorCode {
                case 404:
                    onError("User not found")
                    break
                case 430:
                    onError("You are already logged in. Please sign out of your other device first.")
                    break
                case 401:
                    onError("password not match")
                    break
                default:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                }
            }
        })
    }
    
}

// MARK: Banner/Advertisement API

extension SupremeTVAPI{
    
    class func getAdvertisement(onSuccess: @escaping (_ bannerUrl: String) -> Void, onError: @escaping (String) -> Void) {
        AdvertisementAPI.showAdvertisement(completion: {(data: [Advertisement]? ,_ error: Error?) -> Void in
            if error == nil{
                if data?.count != 0 {
                    onSuccess(data?.first?.image ?? "")
                    return
                }
                onSuccess("")
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
}

// MARK: Draw API

extension SupremeTVAPI{
    
    class func getUserApplyDraw(onSuccess: @escaping (_ data: [UserApplyDrawModel]) -> Void, onError: @escaping (String) -> Void) {
        DrawAPI.getUserApplyDraw(completion: {(data: [UserApplyDrawModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [UserApplyDrawModel]())
            }else{
                onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
            }
        })
    }

    class func userApplyDraw(drawData: DrawToUserModel, onSuccess: @escaping (_ data: DrawToUserModel) -> Void, onError: @escaping (String) -> Void) {
        DrawAPI.userApplyDraw(drawData: drawData, completion: {(data: DrawToUserModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? DrawToUserModel())
            }else{
                let errorCode = error?.errorCode
                switch errorCode {
                case 410:
                    onError("Required feild missing")
                    break
                case 404:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                case 406:
                    onError("You have already applied for this draw.")
                    break
                default:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                }
            }
        })
    }

    class func getDashboardDraw(onSuccess: @escaping (_ data: DrawModel) -> Void, onError: @escaping (String) -> Void) {
        DrawAPI.getDashboardDraw(completion: {(data: DrawModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? DrawModel())
            }else{
                onError("")
            }
        })
    }
    
}

// MARK: Profile API

extension SupremeTVAPI{
    
    class func userLogout(onSuccess: @escaping (_ data: CommonSuccess) -> Void, onError: @escaping (String) -> Void) {
        ProfileAPI.updateUserLogout(completion: {(data: CommonSuccess?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? CommonSuccess())
            }else{
                onError("")
            }
        })
    }
            
    class func updatePassword(updateData: ChangePassword, onSuccess: @escaping (_ data: User) -> Void, onError: @escaping (String) -> Void) {
        ProfileAPI.updateChangePassword(updateDate: updateData, completion: {(data: User?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? User())
            }else{
                let errorCode = error?.errorCode
                switch errorCode {
                case 401:
                    onError("The old password you have entered is incorrect")
                    break
                default:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                }
            }
        })
    }
    
    class func updateProfile(updateDate: User, onSuccess: @escaping (_ data: User) -> Void, onError: @escaping (String) -> Void) {
        ProfileAPI.updateProfile(updateDate: updateDate, completion: {(data: User?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? User())
            }else{
                let errorCode = error?.errorCode
                switch errorCode {
                case 409:
                    onError("Already exist")
                    break
                case 420:
                    onError("Update issue")
                    break
                case 401:
                    onError("password not match")
                    break
                default:
                    onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
                    break
                }
            }
        })
    }
    
    class func uploadUserAvatar(image: URL, onSuccess: @escaping (_ data: CommonSuccess) -> Void, onError: @escaping (String) -> Void) {
        ProfileAPI.uploadUserAvatar(image: image, completion: {(data: CommonSuccess?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? CommonSuccess())
            }else{
                onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
            }
        })
    }
}

// MARK: News API

extension SupremeTVAPI{
    
    class func fetchNews(onSuccess: @escaping (_ newsArray: [NewsModel]) -> Void, onError: @escaping (String) -> Void) {
        NewsAPI.fetchNews(page: nil, perpage: nil, completion: {(data: [NewsModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [NewsModel]())
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
    
    class func fetchNewsBanners(onSuccess: @escaping (_ newsBannerArray: [NewsModel]) -> Void, onError: @escaping (String) -> Void) {
        NewsAPI.fetchNewsBanners(page: nil, perpage: nil, completion: {(data: [NewsModel]?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? [NewsModel]())
            }else{
                onError(error!.localizedDescription)
            }
        })
    }
}

// MARK: Video API

extension SupremeTVAPI{
    
    class func createVideoTitle(withVideoTitle: VideoTitleModel, onSuccess: @escaping (_ videoModel: VideoTitleModel) -> Void, onError: @escaping (String) -> Void) {
        
        VideoAPI.createVideoTitle(data: withVideoTitle, completion: {(data: VideoTitleModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? VideoTitleModel())
            }else{
                onError("")
            }
        })
    }
    
    class func uploadVideo(video: URL, titleId: String, name: String, onSuccess: @escaping (_ videoModel: VideoModel) -> Void, onError: @escaping (String) -> Void) {
        VideoAPI.uploadVideo(video: video, titleId: titleId, name: name, completion: {(data: VideoModel?,_ error: Error?) -> Void in
            if error == nil{
                onSuccess(data ?? VideoModel())
            }else{
                onError(STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE)
            }
        })
    }

}

extension Error {

    var errorCode: Int {
        guard let err = self as? ErrorResponse else { return 2001 }
        switch err{
        case ErrorResponse.error(let code, _, _):
            return code
        }
    }
}
