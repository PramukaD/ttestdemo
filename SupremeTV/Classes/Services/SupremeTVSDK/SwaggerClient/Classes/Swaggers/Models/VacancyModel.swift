//
// VacancyModel.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct VacancyModel: Codable {

    public var _id: String?
    public var title: String?
    public var _description: String?
    public var image: String?
    public var startDate: String?
    public var endDate: String?

    public init(_id: String?, title: String?, _description: String?, image: String?, startDate: String?, endDate: String?) {
        self._id = _id
        self.title = title
        self._description = _description
        self.image = image
        self.startDate = startDate
        self.endDate = endDate
    }

    public enum CodingKeys: String, CodingKey { 
        case _id
        case title
        case _description = "description"
        case image
        case startDate = "start_date"
        case endDate = "end_date"
    }


}

