//
// VideoModel.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct VideoModel: Codable {

    public var _id: String?
    public var user: String?
    public var title: String?
    public var url: String?
    public var activeStatus: Double?
    public var createdDate: String?

    public init(){}

    public init(_id: String?, user: String?, title: String?, url: String?, activeStatus: Double?, createdDate: String?) {
        self._id = _id
        self.user = user
        self.title = title
        self.url = url
        self.activeStatus = activeStatus
        self.createdDate = createdDate
    }

    public enum CodingKeys: String, CodingKey { 
        case _id
        case user
        case title
        case url
        case activeStatus = "active_status"
        case createdDate = "created_date"
    }


}

