//
// TvShowModel.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct TvShowModel: Codable {

    public var _id: String?
    public var title: String?
    public var url: String?
    public var category: CategoryModel?
    public var subCategory: String?
    public var titleImg: String?
    public var coverImg: String?
    public var posterImg: String?
    public var thumbImg: String?
    public var _description: String?
    public var isTrending: Bool?
    public var isBanner: Bool?
    public var showDays: [Double]?
    public var dateTime: String?
    public var ageGroup: String?
    public var showTime: String?
    public var starring: String?
    public var director: String?
    public var producer: String?
    public var amPm: String?
    public var activeStatus: Double?
    public var emptyEpi: Bool?
    public var watchNowUrl: String?
    public var seasons: [SeasonModel]?

    public init(){}

    public init(_id: String?, title: String?, url: String?, category: CategoryModel?, subCategory: String?, titleImg: String?, coverImg: String?, posterImg: String?, thumbImg: String?, _description: String?, isTrending: Bool?, isBanner: Bool?, showDays: [Double]?, dateTime: String?, ageGroup: String?, showTime: String?, starring: String?, director: String?, producer: String?, amPm: String?, activeStatus: Double?, emptyEpi: Bool?, watchNowUrl: String?, seasons: [SeasonModel]?) {
        self._id = _id
        self.title = title
        self.url = url
        self.category = category
        self.subCategory = subCategory
        self.titleImg = titleImg
        self.coverImg = coverImg
        self.posterImg = posterImg
        self.thumbImg = thumbImg
        self._description = _description
        self.isTrending = isTrending
        self.isBanner = isBanner
        self.showDays = showDays
        self.dateTime = dateTime
        self.ageGroup = ageGroup
        self.showTime = showTime
        self.starring = starring
        self.director = director
        self.producer = producer
        self.amPm = amPm
        self.activeStatus = activeStatus
        self.emptyEpi = emptyEpi
        self.watchNowUrl = watchNowUrl
        self.seasons = seasons
    }

    public enum CodingKeys: String, CodingKey { 
        case _id
        case title
        case url
        case category
        case subCategory = "sub_category"
        case titleImg = "title_img"
        case coverImg = "cover_img"
        case posterImg = "poster_img"
        case thumbImg = "thumb_img"
        case _description = "description"
        case isTrending = "is_trending"
        case isBanner = "is_banner"
        case showDays = "show_days"
        case dateTime = "date_time"
        case ageGroup = "age_group"
        case showTime = "show_time"
        case starring
        case director
        case producer
        case amPm = "am_pm"
        case activeStatus = "active_status"
        case emptyEpi = "empty_epi"
        case watchNowUrl = "watch_now_url"
        case seasons
    }


}

