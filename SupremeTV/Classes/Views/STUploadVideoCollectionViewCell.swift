//
//  STUploadVideoCollectionViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/6/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STUploadVideoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var galleryButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleTextField.setLeftPaddingPoints(15.0)
        descriptionTextField.setLeftPaddingPoints(15.0)
        titleTextField.placeholder = "VIDEO_TITLE".localized
        descriptionTextField.placeholder = "DESCRIPTION".localized
    }
}
