//
//  STTVScheduleTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/5/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STTVScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var programmeImage: UIImageView!
    @IBOutlet weak var dateTimeNoLabel: UILabel!
    @IBOutlet weak var programmeNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(aSchedule: ScheduleModel){
        dateTimeNoLabel.text = ((aSchedule.timeFrom!.isEmpty)) ? "" : aSchedule.timeFrom!
        programmeNameLabel.text = ((aSchedule.tvShow?.title!.isEmpty)!) ? "" : aSchedule.tvShow?.title
        descriptionLabel.text = ((aSchedule.tvShow?._description!.isEmpty)!) ? "" : aSchedule.tvShow?._description
        guard let imageUrl =  aSchedule.tvShow?.thumbImg else { return }
        programmeImage.setupImage(imageUrl: imageUrl, imageViewSize: programmeImage.frame.size, placeholderImage: "defalut_thumbnail_S")
        programmeImage.updateConstraintsIfNeeded()
    }
}
