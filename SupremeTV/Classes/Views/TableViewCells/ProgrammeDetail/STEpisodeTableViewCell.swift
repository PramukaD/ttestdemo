//
//  STEpisodeTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STEpisodeTableViewCell: UITableViewCell {

    @IBOutlet weak var episodeImage: UIImageView!
    @IBOutlet weak var episodeNoLabel: UILabel!
    @IBOutlet weak var episodeNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupEpisodeDetails(anEpisode: EpisodeModel){
        episodeNoLabel.text = (anEpisode.name!.isEmpty) ? "" : anEpisode.name
        episodeNameLabel.text = (anEpisode.title!.isEmpty) ? "" : anEpisode.title
        dateLabel.text = (anEpisode.createdDate == nil) ? "" : anEpisode.createdDate?.formattedProgrammeDate()
        guard let imageUrl =  anEpisode.coverImg else { return }
        episodeImage.setupImage(imageUrl: imageUrl, imageViewSize: episodeImage.frame.size, placeholderImage: "defalut_thumbnail_S")
        episodeImage.updateConstraintsIfNeeded()
    }

}
