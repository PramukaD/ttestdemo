//
//  STProgrammeDetailBannerTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import DPArrowMenuKit

class STProgrammeDetailBannerTableViewCell: UITableViewCell {

    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var starringLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var producerLabel: UILabel!

    @IBOutlet weak var watchButton: UIButton!
    @IBOutlet weak var seasonButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var stLabel: UILabel!
    @IBOutlet weak var diLabel: UILabel!
    @IBOutlet weak var proLabel: UILabel!

    @IBOutlet weak var seasonButtonBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var starringTopSpace: NSLayoutConstraint!
    @IBOutlet weak var directorTopSpace: NSLayoutConstraint!
    @IBOutlet weak var producerTopSpace: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        seasonButton.setButtonGradient(width:125.0)
        seasonButton.clipsToBounds = true
    }
    
    func setupCellInfo(aProgramme: TvShowModel){
        titleLabel.text = aProgramme.title?.trim()
        dateLabel.text = "\(aProgramme.dateTime ?? "") \(aProgramme.showTime?.getTime() ?? "")"
        descriptionLabel.text = aProgramme._description?.trim()
        
        if aProgramme.starring?.isEmpty ?? false || aProgramme.starring == nil{
            stLabel.isHidden = true
            starringLabel.isHidden = true
            self.starringTopSpace.constant = 0
        }else{
            stLabel.isHidden = false
            starringLabel.isHidden = false
            starringLabel.text = aProgramme.starring?.trim()
            self.starringTopSpace.constant = 20
        }
        if aProgramme.director?.isEmpty ?? false || aProgramme.director == nil{
            directorLabel.isHidden = true
            diLabel.isHidden = true
            self.directorTopSpace.constant = 0
        }else{
            directorLabel.isHidden = false
            diLabel.isHidden = false
            directorLabel.text = aProgramme.director?.trim()
            self.directorTopSpace.constant = 15
        }
        if aProgramme.producer?.isEmpty ?? false || aProgramme.producer == nil{
            producerLabel.isHidden = true
            proLabel.isHidden = true
            self.producerTopSpace.constant = 0
        }else {
            producerLabel.isHidden = false
            proLabel.isHidden = false
            producerLabel.text = aProgramme.producer?.trim()
            self.producerTopSpace.constant = 15
        }
        guard let imageUrl =  aProgramme.coverImg else { return }
        bannerImage.setupImage(imageUrl: imageUrl, imageViewSize: bannerImage.frame.size, placeholderImage: "placeholderthumbnail_L")
        bannerImage.updateConstraintsIfNeeded()
    }
    
    func displaySeasonButton(title: String, menuArray: [DPArrowMenuViewModel]){
        seasonButton.isHidden =  (menuArray.count >= 1) ? false : true
        arrowImage.isHidden =  (menuArray.count >= 2) ? false : true
        seasonButton.setTitle(title, for: .normal)
    }
}
