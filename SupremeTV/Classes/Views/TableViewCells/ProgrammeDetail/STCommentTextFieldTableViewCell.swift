//
//  STCommentTextFieldTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STCommentTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logincommentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentLabel.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        commentTextField.setLeftPaddingPoints(15.0)
        commentTextField.placeholder = "WRITE_COMMENT".localized
        postButton.setButtonGradient(width:80.0)
        postButton.clipsToBounds = true
    }
    
    func displayCommentLabel(isShown: Bool){
        self.commentLabel.isHidden = isShown
    }
    
}

class STLoginCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var loginButton: UIButton!

    @IBOutlet weak var commentViewHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func displayCommentLabel(isShown: Bool){
        loginButton.setAttributeText(title: "PLEASE_LOGIN_TO_COMMENT".localized, ftitle: "LOGIN_SPACE".localized)
        self.commentView.isHidden = isShown
        self.commentViewHeight.constant = (isShown) ? 0 : 45.0
    }

}
