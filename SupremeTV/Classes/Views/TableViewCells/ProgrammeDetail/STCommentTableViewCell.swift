//
//  STCommentTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCommentCell(aComment: ShowCommentModel){
        usernameLabel.text = ("\(aComment.user?.firstName ?? "") \(aComment.user?.lastName ?? "")")
        commentLabel.text = aComment.comment
        dateLabel.text = (aComment.createdDate == nil) ? "" : aComment.createdDate?.formattedProgrammeDate()
        guard let imageUrl =  aComment.user?.image else {
            if let gen =  aComment.user?.gender{
                profileImage.image = UIImage(named: (gen == 1) ? "avatar_man_s" : "avatar_woman_s")
                profileImage.contentMode = .scaleAspectFill
            }
            return
        }
        profileImage.setupImage(imageUrl: imageUrl, imageViewSize: profileImage.frame.size, placeholderImage: "default_avatar_s")
        profileImage.updateConstraintsIfNeeded()
    }
   
}
