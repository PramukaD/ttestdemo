//
//  STSideMenuTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STSideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var Icon: UIImageView!
    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet var socialButtons: [UIButton]!
    
    @IBOutlet weak var logoTopSpace: NSLayoutConstraint!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(indexPath: IndexPath, menu: STSideMenu){
        self.logoTopSpace.constant = 90.0
        self.Icon.isHidden = false
        self.menuTitle.isHidden = false
        let anImage = UIImage.init(named: menu.icon!)
        self.Icon.image = anImage
        self.menuTitle.text = menu.menuTitle
    }
    
    func setupSocialMediaCell(){
        self.logoTopSpace.constant = 20.0
        self.Icon.isHidden = true
        self.menuTitle.isHidden = true
    }
}
