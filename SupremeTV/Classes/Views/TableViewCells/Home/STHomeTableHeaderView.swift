//
//  STHomeTableHeaderView.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/1/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STHomeTableHeaderView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var titleLabelCenter: NSLayoutConstraint!
    @IBOutlet weak var underlineButon: UIButton!

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    func setLabelWidth(headerText : String) {
        self.titleLabel.font =  UIFont.boldSystemFont(ofSize: 17.0)
        self.titleLabel.text = headerText
        self.titleLabelWidth.constant =  self.titleLabel.intrinsicContentSize.width + 30
        underlineButon.setButtonGradient(width: self.titleLabel.intrinsicContentSize.width + 30)
        underlineButon.clipsToBounds = true
    }
    
    func configureDrawHeader(){
        underlineButon.isHidden = true
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        self.titleLabel.text = "MY_DRAWS".localized
        self.titleLabelCenter.constant = 0
    }
}
