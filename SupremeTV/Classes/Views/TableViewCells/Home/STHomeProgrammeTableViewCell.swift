//
//  STHomeProgrammeTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

protocol STHomeProgrammeTableViewCellDelegate: class {
    func didSelectProgrammeIndexPath(indexPath: IndexPath, section: Int)
}

class STHomeProgrammeTableViewCell: UITableViewCell {

    weak var delegate: STHomeProgrammeTableViewCellDelegate?

    @IBOutlet weak var programmeCollectionView: UICollectionView!
    @IBOutlet weak var bannerImageView: UIImageView!

    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!

    var programmeArray = [TvShowModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.programmeCollectionView.register(UINib(nibName: "STHomeProgrammeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STHomeProgrammeCollectionViewCell")
        self.programmeCollectionView.delegate = self
        self.programmeCollectionView.dataSource = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.collectionViewHeight.constant = (UIDevice.current.screenType == .iPhones_5_5s_5c_SE_4_4S) ? 125.0 : 145.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
            // Configure the view for the selected state
    }
        
    func reloadProgrammeCollectionView(programmes : [TvShowModel],section: Int, bannerSection: Int){
        self.bannerImageView.isHidden = (section == bannerSection) ? false : true
        self.programmeCollectionView.tag = section
        self.programmeArray = programmes
        self.programmeCollectionView.reloadData()
    }
    
    func setupBannerView(imageUrl: String){
        self.bannerImageView.setupImage(imageUrl: imageUrl, imageViewSize: self.bannerImageView.frame.size, placeholderImage: "")
        self.bannerImageView.updateConstraintsIfNeeded()
    }
}

// MARK: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension STHomeProgrammeTableViewCell : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return programmeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STHomeProgrammeCollectionViewCell", for: indexPath) as! STHomeProgrammeCollectionViewCell
        cell.setupProgrammeDetails(programme: programmeArray[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        delegate?.didSelectProgrammeIndexPath(indexPath: indexPath, section: collectionView.tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            return CGSize(width: 160, height: 125)
        }
        return CGSize(width: 185, height: 145)
    }
}
