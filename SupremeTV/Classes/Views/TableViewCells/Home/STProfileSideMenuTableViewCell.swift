//
//  STProfileSideMenuTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STProfileSideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var signupView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(){
        if UserDefaults.standard.bool(forKey: STConstant.STUserDefaultsKeys.IS_USER_LOGGEDIN){
            signupView.isHidden = true
            profileView.isHidden = false
            let user = STUtils.getUser()
            self.usernameLabel.text = "\(user.firstName ?? "") \(user.lastName ?? "")"
            self.emailLabel.text = user.email
            guard let imageUrl =  user.image else {
                if let gen =  user.gender{
                    profileImageView.image = UIImage(named: (gen == 1) ? "avatar_woman_100px" : "avatar_man_100px")
                    profileImageView.contentMode = .scaleAspectFill
                }
                return
            }
            profileImageView.setupImage(imageUrl: imageUrl, imageViewSize: profileImageView.frame.size, placeholderImage: "default_avatar_100px")
            profileImageView.updateConstraintsIfNeeded()
        }else{
            profileView.isHidden = true
            signupView.isHidden = false
            loginButton.setAttributeText(title: "ALREADY_MEMBER_OF_SUPREMETV".localized, ftitle: "LOGIN_SPACE".localized)
            signupButton.setButtonGradient(width: 100.0)
            signupButton.clipsToBounds = true
            loginButton.contentHorizontalAlignment = .left
        }
    }
    
}

