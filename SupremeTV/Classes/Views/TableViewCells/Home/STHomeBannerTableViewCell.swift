//
//  STHomeBannerTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

protocol STHomeBannerTableViewDelegate: class {
    func didSelectBannerIndex(index: Int)
}

class STHomeBannerTableViewCell: UITableViewCell, UIScrollViewDelegate {

    weak var delegate:STHomeBannerTableViewDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var programmeNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var watchNowButon: UIButton!
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var bannerContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    @IBOutlet weak var liveButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var bannerViewHeight: NSLayoutConstraint!

    @IBOutlet var topBottomSpace: [NSLayoutConstraint]!
    var bannerSlidesArray = [STHomeSlide]()
    var tvShowBannerArray = [TvShowModel]()
    var bannerIndex = 0
    
    var bannerHeight: CGFloat{
        return (UIDevice.current.screenType == .iPhones_5_5s_5c_SE_4_4S) ? 200.0 : (UIDevice.current.screenType == .iPhones_6_6s_7_8) ? 235.0 : 260.0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.bannerViewHeight.constant = bannerHeight
        liveButton.clipsToBounds = true
        watchNowButon.setButtonGradient(width:110.0)
        watchNowButon.clipsToBounds = true
        setupNowShowing()
    }
    
    @IBAction func watchButtonAction(_ sender: Any) {
        delegate?.didSelectBannerIndex(index: self.bannerIndex)
    }
}

// MARK: Configure Banner views

extension STHomeBannerTableViewCell{
    
    func setupBanerView(tvShowArray: [TvShowModel]){
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        if self.tvShowBannerArray.count != 0 { self.tvShowBannerArray.removeAll() }
        if self.bannerSlidesArray.count != 0 { self.bannerSlidesArray.removeAll() }
        self.layoutIfNeeded()
        self.tvShowBannerArray = tvShowArray
        self.createSlides()
        self.setupIntroSlideScrollView(slides: bannerSlidesArray)
        setupNowShowing()
        watchNowButon.isHidden = (self.tvShowBannerArray.count == 0) ? true : false
    }
    
    func setupNowShowing(){
        self.titleLabel.text = ""
        self.programmeNameLabel.text = ""
        self.timeLabel.text = ""
        for aConstraint in self.topBottomSpace{
            aConstraint.constant = 5.0
        }
        self.liveButtonHeight.constant = 0.0
        self.updateConstraintsIfNeeded()
    }
    
    func displayNowShowingView(){
        self.titleLabel.isHidden = true
        self.programmeNameLabel.isHidden = true
        self.timeLabel.isHidden = true
        self.arrowImage.isHidden = true
    }
    
    private func createSlides() {
        for aSlide in self.tvShowBannerArray{
            let bannerView = Bundle.main.loadNibNamed("STHomeSlide", owner: self, options: nil)?.first as! STHomeSlide
            bannerView.layoutIfNeeded()
            setupBannerImage(bannerView: bannerView, tvShow: aSlide)
            setupBannerTitleImage(bannerView: bannerView, tvShow: aSlide)
            self.bannerSlidesArray.append(bannerView)
        }
    }
    
    private func setupBannerImage(bannerView: STHomeSlide, tvShow: TvShowModel){
        guard let imageUrl =  tvShow.coverImg else { return }
        bannerView.homeSlideImageView.setupImage(imageUrl: imageUrl, imageViewSize: bannerView.homeSlideImageView.frame.size, placeholderImage: "defalut_thumbnail_L")
        bannerView.homeSlideImageView.updateConstraintsIfNeeded()
    }
    
    private func setupBannerTitleImage(bannerView: STHomeSlide, tvShow: TvShowModel){
        guard let imageUrl =  tvShow.titleImg else { return }
        bannerView.titleImageView.setupImage(imageUrl: imageUrl, imageViewSize: bannerView.titleImageView.frame.size, placeholderImage: "")
        bannerView.titleImageView.updateConstraintsIfNeeded()
    }
    
    private func setupIntroSlideScrollView(slides: [STHomeSlide]) {
        let widthh = UIScreen.main.nativeBounds.width / UIScreen.main.nativeScale
        let height = bannerHeight
        self.scrollView.frame = CGRect(x: 0, y: 0, width: widthh, height: height)
        self.scrollView.contentSize = CGSize(width: widthh * CGFloat(slides.count), height: height)
        self.scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            let sli = slides[i]
            sli.frame = CGRect(x: widthh * CGFloat(i), y: 0, width:widthh, height: height)
            sli.layoutIfNeeded()
            self.scrollView.addSubview(slides[i])
            self.layoutIfNeeded()
        }
    }
}

// MARK: UIScrollViewDelegate

extension STHomeBannerTableViewCell {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.bannerIndex = Int(round(scrollView.contentOffset.x / bannerContainerView.frame.width))
    }
    
}
