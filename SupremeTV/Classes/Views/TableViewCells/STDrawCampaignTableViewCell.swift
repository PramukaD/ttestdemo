//
//  STDrawCampaignTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/15/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STDrawCampaignTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var campaignStatusLabel: UILabel!
    @IBOutlet weak var qrButton: UIButton!
    @IBOutlet weak var qrImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureDrawCell(aDraw: DrawModel, dashboardStatus: STDrawStatus){
        self.qrImageView.image = UIImage.init(named: "")
        self.campaignStatusLabel.text = (aDraw.name == nil) ? "NO_ACTIVATE_CAMPAIGNS_AVAILABLE".localized : "\("ACTIVE_CAMPAIGN".localized): \(aDraw.name!)"
        let eDate = "ENTRY_DATE".localized + ":"
        let eTime = "ENTRY_TIME".localized + ":"
        self.dateLabel.text = (aDraw.endDate == nil) ? eDate : "\(eDate) \(aDraw.endDate!)"
        self.timeLabel.text = (aDraw.endTime == nil) ? eTime : "\(eTime) \(aDraw.endTime?.getTime() ?? "")"
        if dashboardStatus == .applied {
            self.qrButton.isEnabled = false
            self.messageLabel.font = UIFont.systemFont(ofSize: 15.0)
            self.messageLabel.textColor = .white
            self.messageLabel.text = "YOUR_ENTRY_HAS_SUCCESSFULLY_SUBMITTED".localized
            self.qrImageView.image = UIImage.init(named: "qr-code-inactive")
        }else{
            self.messageLabel.font = UIFont.systemFont(ofSize: 12.0)
            self.messageLabel.textColor = UIColor.init(hexString: "#ABABAB")
            self.messageLabel.text = "SCAN_THE_SUPREME_TV_QR_CODE_TO_PARTICIPATE".localized
            if dashboardStatus == .stdefault{
                self.qrButton.isEnabled = false
                self.qrImageView.image = UIImage.init(named: "qr-code-inactive")
            }else if dashboardStatus == .notapplied {
                self.qrButton.isEnabled = true
                self.qrImageView.image = UIImage.init(named: "qr-code-active")
            }
        }
         self.qrImageView.setNeedsDisplay()
    }

}
