//
//  STMyDrawsTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/9/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STMyDrawsTableViewCell: UITableViewCell {

    @IBOutlet weak var drawNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var emptyView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureDrawCell(aDraw: UserApplyDrawModel){
        self.emptyView.isHidden = true
        switch Int(aDraw.status ?? "4") {
        case 1:
            statusButton.setTitle("PENDING".localized, for: .normal)
            statusButton.backgroundColor = UIColor.init(hexString: "#feb838")
            break
        case 2:
            statusButton.setTitle("TRY_AGAIN".localized, for: .normal)
            statusButton.backgroundColor = UIColor.init(hexString: "#ff5858")
            break
        case 3:
            statusButton.setTitle("WINNER".localized, for: .normal)
            statusButton.backgroundColor = UIColor.init(hexString: "#05b982")
            break
        default:
            statusButton.setTitle("", for: .normal)
            statusButton.backgroundColor = UIColor.clear
            break
        }
        self.drawNameLabel.text = (aDraw.draw?.name == nil) ? "" : aDraw.draw?.name
        let (date, time) = self.getDateTime(dateS: aDraw.createdDate ?? "")
        self.dateLabel.text = date
        self.timeLabel.text = time
    }
    
    private func getDateTime(dateS: String) -> (String, String) {
        let eDate = "ENTRY_DATE".localized + ":"
        let eTime = "ENTRY_TIME".localized + ":"
        if dateS.isEmpty {
            return (eDate, eTime)
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: dateS)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formattedDate = dateFormatter.string(from: date!)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        return ("\(eDate) \(formattedDate)", "\(eTime) \(Date12)")
    }
    
    func configureEmptyDraw(){
        self.emptyView.isHidden = false
    }
}
