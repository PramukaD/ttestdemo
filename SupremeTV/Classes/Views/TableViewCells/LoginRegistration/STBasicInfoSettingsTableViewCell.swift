//
//  STBasicInfoSettingsTableViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/29/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

protocol STBasicInfoTableViewCellDelegate: class {
    func didSelectGender(gender: Bool)
}

class STBasicInfoSettingsTableViewCell: UITableViewCell {

    weak var delegate: STBasicInfoTableViewCellDelegate?

    @IBOutlet weak var fnameTextField: UITextField!
    @IBOutlet weak var lnameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!

    @IBOutlet weak var dobLabel: UILabel!

    @IBOutlet weak var profileImageView: UIImageView!

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var dobButton: UIButton!

    @IBOutlet var buttonsCollection: [UIButton]!
    @IBOutlet weak var photoButton: UIButton!

    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureBasicInfoSettingsView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func configureBasicInfoSettingsView(){
        fnameTextField.setLeftPaddingPoints(15.0)
        lnameTextField.setLeftPaddingPoints(15.0)
        mobileTextField.setLeftPaddingPoints(15.0)
        emailTextField.setLeftPaddingPoints(15.0)
        saveButton.setButtonGradient(width:UIScreen.main.bounds.width - 40)
        saveButton.clipsToBounds = true
        dobButton.clipsToBounds = true
        for aButton in buttonsCollection{
            aButton.clipsToBounds = true
        }
        self.maleButton.tintColor = UIColor.init(hexString: "#faab19")
        self.femaleButton.tintColor = UIColor.init(hexString: "#faab19")
        
        fnameTextField.placeholder = "FIRST_NAME".localized
        lnameTextField.placeholder = "LAST_NAME".localized
        mobileTextField.placeholder = "PHONE_NUMBER".localized
        emailTextField.placeholder = "EMAIL".localized
    }
    
    func setupProfileCell(profileInfo: STProfilrInfo){
        fnameTextField.text = profileInfo.fName
        lnameTextField.text = profileInfo.lNama
        mobileTextField.text = profileInfo.mobileNumber
        emailTextField.text = profileInfo.email
        if profileInfo.dob.isEmpty{
            dobLabel.text = "DATE_OF_BIRTHDAY".localized
        }else{
            dobLabel.text = profileInfo.dob
            dobLabel.textColor = .white
        }
        
        self.setupUserGender(gender: profileInfo.gender)

        if profileInfo.profileImageStringUrl == "" {
            self.setupImage(gender: profileInfo.gender)
        }else{
            profileImageView.setupImage(imageUrl: profileInfo.profileImageStringUrl, imageViewSize: profileImageView.frame.size, placeholderImage: "default_avatar_100px")
            profileImageView.updateConstraintsIfNeeded()
        }
    }
    
    @IBAction func genderRadioButtonAction(_ sender: Any) {
        let tappedButton = sender as! UIButton
        for aButton in self.buttonsCollection {
            if aButton.tag == tappedButton.tag {
                aButton.setImage(UIImage.init(named: "ic_radio_select"), for: .normal)
                aButton.borderWidth = 0
            }else{
                aButton.setImage(UIImage.init(named: ""), for: .normal)
                aButton.borderColor = UIColor.init(hexString: "#747474")
                aButton.borderWidth = 1.0
            }
        aButton.clipsToBounds = true
        }
        delegate?.didSelectGender(gender: (tappedButton.tag == 20) ? true : false)
    }
    
    func setupImage(gender: Int){
        if gender != 2{
            profileImageView.image = UIImage.init(named: (gender == 1) ? "avatar_woman_100px" : "avatar_man_100px")
        }
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.updateConstraintsIfNeeded()
    }
    
    private func setupUserGender(gender: Int){
        if gender == 1{
            self.femaleButton.setImage(UIImage.init(named: "ic_radio_select"), for: .normal)
            self.maleButton.borderColor = UIColor.init(hexString: "#747474")
            self.maleButton.borderWidth = 1.0
        }else if gender == 0 {
            self.maleButton.setImage(UIImage.init(named: "ic_radio_select"), for: .normal)
            self.femaleButton.borderColor = UIColor.init(hexString: "#747474")
            self.femaleButton.borderWidth = 1.0
        }else{
            self.maleButton.borderColor = UIColor.init(hexString: "#747474")
            self.maleButton.borderWidth = 1.0
            self.femaleButton.borderColor = UIColor.init(hexString: "#747474")
            self.femaleButton.borderWidth = 1.0
        }
    }
}
