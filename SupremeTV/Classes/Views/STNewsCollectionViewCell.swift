//
//  STNewsCollectionViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/12/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STNewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var programmeImageView: UIImageView!
    @IBOutlet weak var programmeTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var programmeImageViewHeight: NSLayoutConstraint!

    func setupNewsDetails(aNews: NewsModel){
        self.programmeTitleLabel.text = aNews.title
        self.dateLabel.text = (aNews.createdDate == nil) ? "" : aNews.createdDate?.formattedProgrammeDate()
        guard let imageUrl = aNews.thumbImg else { return }
        self.programmeImageView.setupImage(imageUrl: imageUrl, imageViewSize: self.programmeImageView.frame.size, placeholderImage: "defalut_thumbnail_S")
        self.programmeImageView.updateConstraintsIfNeeded()
    }
    
    func setupProgrammeImageViewHeight(){
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            programmeImageViewHeight.constant = 85.0
            return
        }
        programmeImageViewHeight.constant = 105.0
        self.updateFocusIfNeeded()
    }
}
