//
//  STHomeSlide.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STHomeSlide: UIView {

    @IBOutlet weak var homeSlideImageView: UIImageView!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

}
