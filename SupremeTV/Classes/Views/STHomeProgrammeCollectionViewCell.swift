//
//  STHomeProgrammeCollectionViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STHomeProgrammeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var programmeImageView: UIImageView!
    @IBOutlet weak var programmeTitleLabel: UILabel!
    @IBOutlet weak var programmeImageViewHeight: NSLayoutConstraint!

    @IBOutlet weak var closeImageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    
    func setupProgrammeDetails(programme: TvShowModel){
        self.closeImageView.isHidden = true
        self.closeButton.isHidden = true
        self.programmeTitleLabel.text = programme.title
        guard let imageUrl = programme.thumbImg else { return }
        self.programmeImageView.setupImage(imageUrl: imageUrl, imageViewSize: self.programmeImageView.frame.size, placeholderImage: "defalut_thumbnail_S")
        self.programmeImageView.updateConstraintsIfNeeded()
    }
    
    func setupProgrammeImageViewHeight(){
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            programmeImageViewHeight.constant = 85.0
            return
        }
        programmeImageViewHeight.constant = 105.0
        self.updateFocusIfNeeded()
    }
    
    func setTitleTextAlignment(){
        self.programmeTitleLabel.textAlignment = .center
    }
    
    func setupVideoDetails(aVideo: STVideoUpload){
        self.closeImageView.isHidden = false
        self.closeButton.isHidden = false
        self.programmeTitleLabel.text = aVideo.title
        self.programmeImageView.image =  aVideo.thumbnail
    }
}

