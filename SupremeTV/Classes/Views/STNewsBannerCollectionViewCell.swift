//
//  STNewsBannerCollectionViewCell.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/12/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

protocol STNewsBannerCollectionViewDelegate: class {
    func didSelectNewsBannerIndex(index: Int)
}

class STNewsBannerCollectionViewCell: UICollectionViewCell {
    
    weak var delegate:STNewsBannerCollectionViewDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var watchButon: UIButton!
    @IBOutlet weak var bannerContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet{
            scrollView.delegate = self
        }
    }
    
    var newsBannerArray = [NewsModel]()
    var bannerSlidesArray = [STHomeSlide]()
    var bannerIndex = 0

    override func layoutSubviews() {
        super.layoutSubviews()
        watchButon.setButtonGradient(width:110.0)
        watchButon.clipsToBounds = true
    }
        
    @IBAction func watchButtonAction(_ sender: Any) {
        delegate?.didSelectNewsBannerIndex(index: self.bannerIndex)
    }
}

// MARK: Configure News Banner views

extension STNewsBannerCollectionViewCell{
    
    func setupBanerView(bannerArray: [NewsModel]){
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
        if self.newsBannerArray.count != 0 { self.newsBannerArray.removeAll() }
        if self.bannerSlidesArray.count != 0 { self.bannerSlidesArray.removeAll() }
        self.layoutIfNeeded()
        self.newsBannerArray = bannerArray
        self.createSlides()
        self.setupIntroSlideScrollView(slides: bannerSlidesArray)
    }
    
    private func createSlides() {
        for aSlide in self.newsBannerArray{
            let bannerView = Bundle.main.loadNibNamed("STHomeSlide", owner: self, options: nil)?.first as! STHomeSlide
            setupBannerImage(bannerView: bannerView, aNews: aSlide)
            self.bannerSlidesArray.append(bannerView)
        }
    }
    
    private func setupBannerImage(bannerView: STHomeSlide, aNews: NewsModel){
        bannerView.titleLabel.text = aNews.title
        bannerView.titleLabel.textColor = .white
        guard let imageUrl =  aNews.coverImg else { return }
        bannerView.homeSlideImageView.setupImage(imageUrl: imageUrl, imageViewSize: bannerView.homeSlideImageView.frame.size, placeholderImage: "defalut_thumbnail_L")
        bannerView.homeSlideImageView.updateConstraintsIfNeeded()
    }
    
    private func setupIntroSlideScrollView(slides: [STHomeSlide]) {
        self.scrollView.frame = CGRect(x: 0, y: 0, width: bannerContainerView.frame.width, height: bannerContainerView.frame.height)
        self.scrollView.contentSize = CGSize(width: bannerContainerView.frame.width * CGFloat(slides.count), height: bannerContainerView.frame.height)
        self.scrollView.isPagingEnabled = true
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: bannerContainerView.frame.width * CGFloat(i), y: 0, width: bannerContainerView.frame.width, height: bannerContainerView.frame.height)
            self.scrollView.addSubview(slides[i])
        }
    }
    
}

// MARK: UIScrollViewDelegate

extension STNewsBannerCollectionViewCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.bannerIndex = Int(round(scrollView.contentOffset.x / bannerContainerView.frame.width))
    }
    
}
