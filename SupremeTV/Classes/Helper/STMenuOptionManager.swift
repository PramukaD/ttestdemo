//
//  STMenuOptionManager.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STMenuOptionManager: NSObject {

    static var sharedInstance = STMenuOptionManager()
    var slidingPanel: SVSlidingPanelViewController
    
    override init() {
        self.slidingPanel = SVSlidingPanelViewController()
        super.init()
        setupRevealCtrl()
    }
    
    public func setupRevealCtrl(){
        UserDefaults.standard.setValue(0, forKey: STConstant.STUserDefaultsKeys.SIDE_MENU_SELECTED_INDEX)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let  leftHamburgerMenuController : STSideMenuViewController = storyboard.instantiateViewController(withIdentifier: "STSideMenuViewController") as! STSideMenuViewController
        let  detailController : STHomeViewController = storyboard.instantiateViewController(withIdentifier: "STHomeViewController") as! STHomeViewController
        let navigation = UINavigationController(rootViewController:detailController)
        self.slidingPanel.leftPanel = leftHamburgerMenuController
        self.slidingPanel.centerPanel = navigation
        self.slidingPanel.hamburgurButtonForLeftPanel()
        leftHamburgerMenuController.menuSelectedIndexPath = {[weak self](indexPath: IndexPath) in
            self?.showSelectedMenuItemViewWithIndexPath(indexPath: indexPath)
        }
        leftHamburgerMenuController.profileLoginRegistration = {[weak self](isLogin: Bool) in
            UserDefaults.standard.setValue(0, forKey: STConstant.STUserDefaultsKeys.SIDE_MENU_SELECTED_INDEX)
            self?.profileLoginRegistration(isLogin: isLogin)
        }
    }
    
    private func showSelectedMenuItemViewWithIndexPath(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var detailController = UIViewController()
        if indexPath.row == 0 || indexPath.row == 1 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STHomeViewController") as! STHomeViewController
            detailController = viewCtrl
        }else if indexPath.row == 2 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STWatchLiveViewController") as! STWatchLiveViewController
            detailController = viewCtrl
        }else if indexPath.row == 3 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STTVScheduleViewController") as! STTVScheduleViewController
            detailController = viewCtrl
        }else if indexPath.row == 4 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STProfileSettingsViewController") as! STProfileSettingsViewController
            detailController = viewCtrl
        }
        else if indexPath.row == 5 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STMyDrawsViewController") as! STMyDrawsViewController
            detailController = viewCtrl
        }else if indexPath.row == 6 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STContactViewController") as! STContactViewController
            detailController = viewCtrl
        }
        else if indexPath.row == 7 {
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STUploadVideoViewController") as! STUploadVideoViewController
            detailController = viewCtrl
        }
        let navigation = UINavigationController(rootViewController:detailController)
        self.slidingPanel.centerPanel = navigation
        self.slidingPanel.showCenterPanel(animated: true)
    }
    
    private func profileLoginRegistration(isLogin: Bool){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var detailController = UIViewController()
        if isLogin{
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STLoginViewController") as! STLoginViewController
            detailController = viewCtrl
        }else{
            let viewCtrl = storyboard.instantiateViewController(withIdentifier: "STSignupViewController") as! STSignupViewController
            detailController = viewCtrl
        }
        let navigation = UINavigationController(rootViewController:detailController)
        self.slidingPanel.centerPanel = navigation
        self.slidingPanel.showCenterPanel(animated: true)
    }

}
