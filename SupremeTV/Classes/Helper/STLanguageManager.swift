//
//  STLanguageManager.swift
//  SupremeTV
//
//  Created by Persystance Networks on 1/6/20.
//  Copyright © 2020 Persystance Networks. All rights reserved.
//

import UIKit

let APPLE_LANGUAGE_KEY = "AppleLanguages"

enum STLanguage {
    case english
    case sinhala
    case tamil
}

struct STLanguagesStruct {
    var id :Int
    var type :STLanguage
    var key :String
    var title :String
    var isCurrent :Bool
}

class STLanguageManager: NSObject {
    var languagesData :[STLanguagesStruct] = [
        STLanguagesStruct(id: 40, type: .english, key: "en", title: "English", isCurrent: true),
        STLanguagesStruct(id: 41, type: .sinhala, key: "si", title: "සිංහල", isCurrent: false),
    ]
    
    static var shared :STLanguageManager = {
        let model = STLanguageManager()
        model.updateCurrentLanguage()
        model.updateLanguagesData(lang: model.currentLanguage)
        return model
    }()
    
    var currentLanguage :STLanguagesStruct = STLanguagesStruct(id: 40, type: .english, key: "en", title: "English", isCurrent: true)
    
    func changeLanguage(lang: STLanguagesStruct) {
        setAppleLAnguageTo(lang: lang.key)
        updateLanguagesData(lang: lang)
        STLanguageManager.shared.updateCurrentLanguage()
    }
    
    func updateLanguagesData(lang: STLanguagesStruct) {
        var idx = 0
        for item in languagesData {
            languagesData[idx].isCurrent = item.key == lang.key
            idx += 1
        }
    }
    
    func updateCurrentLanguage() {
        let langKey :String = currentAppleLanguage()
        if let data = languagesData.first(where: { (ls) -> Bool in
            return ls.key == langKey
        }) {
            self.currentLanguage = data
        } else {
            self.currentLanguage = languagesData[0]
        }
    }
    
    /// get current Apple language
    func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    /// set @lang to be the first in Applelanguages list
    func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
    
    public func updateView(rootViewController: UIViewController? = nil, animation: ((UIView) -> Void)? = nil) {
        guard let rootViewController = rootViewController else {
            return
        }
        let snapshot = (UIApplication.shared.keyWindow?.snapshotView(afterScreenUpdates: true))!
        rootViewController.view.addSubview(snapshot);
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootViewController
        UIView.animate(withDuration: 0.5, animations: {
            animation?(snapshot)
        }) { _ in
            snapshot.removeFromSuperview()
        }
        
    }
}
