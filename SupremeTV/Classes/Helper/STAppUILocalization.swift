//
//  STAppUILocalization.swift
//  SupremeTV
//
//  Created by Persystance Networks on 1/6/20.
//  Copyright © 2020 Persystance Networks. All rights reserved.
//

import UIKit

extension UITextField {
    @IBInspectable var localizedPlaceholder : String? {
        set (newValue) {
            self.placeholder = newValue?.localized
        }
        get {
            return self.placeholder
        }
    }
}

extension UILabel {
    @IBInspectable var localizedText : String? {
        set (newValue) {
            self.text = newValue?.localized
        }
        get {
            return self.text
        }
    }
}
extension UIButton {
    @IBInspectable var localizedText : String? {
        set (newValue) {
            self.setTitle(newValue?.localized, for: UIControl.State())
        }
        get {
            return self.titleLabel?.text
        }
    }
}

extension UITextView {
    @IBInspectable var localizedText : String? {
        set (newValue) {
            self.text = newValue?.localized
        }
        get {
            return self.text
        }
    }
}

extension UIBarButtonItem {
    @IBInspectable var localizedText : String? {
        set (newValue) {
            self.title = newValue?.localized
        }
        get {
            return self.title
        }
    }
}

extension UINavigationItem {
    @IBInspectable var localizedText : String? {
        set (newValue) {
            self.title = newValue?.localized
        }
        get {
            return self.title
        }
    }
}

extension UITabBarItem {
    @IBInspectable var localizedText : String? {
        set (newValue) {
            self.title = newValue?.localized
        }
        get {
            return self.title
        }
    }
}

extension String {
    var localized :String {
        get {
            guard let bundle = Bundle.main.path(forResource: STLanguageManager.shared.currentLanguage.key, ofType: "lproj"), let langBundle = Bundle(path: bundle) else {
                return NSLocalizedString(self, comment: "")
            }
            let newString = self.trim().replacingOccurrences(of: " ", with: "_", options: .literal, range: nil).uppercased()
            return NSLocalizedString(newString, tableName: nil, bundle: langBundle, comment: "")
        }
    }
}

extension UILabel {
    func localize(_ text :String) {
        self.text = text.localized
    }
}


