//
//  STGoogleFacebookSignIn.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/2/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import FBSDKLoginKit
import Firebase
import FacebookCore
import AuthenticationServices

protocol STGoogleFacebookSignInDelegate: class {
    func socialLoginToken(idToken: String)
}

class STGoogleFacebookSignIn: NSObject  {

    weak var delegate: STGoogleFacebookSignInDelegate?
    var viewCtrl = UIViewController()
    var firstName: String?
    var lastName: String?

    func firebaseSigin(credential: AuthCredential){
        viewCtrl.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if (error != nil) { self.viewCtrl.stopActicityAnimating(); return}
            let currentUser = Auth.auth().currentUser
            currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                self.viewCtrl.stopActicityAnimating()
                if (error != nil) { return; }
                self.delegate?.socialLoginToken(idToken: idToken!)
            }
        }
    }
    
}

extension STGoogleFacebookSignIn: GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil { return }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        self.firebaseSigin(credential: credential)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print(error.localizedDescription)
    }
}

extension STGoogleFacebookSignIn{
    
    func doFacebookLogin(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: nil) { loginResult in
            switch loginResult {
            case .failed: break
            case .cancelled: break
            case .success( _, _, _):
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                self.firebaseSigin(credential: credential)
            }
        }
    }

}

@available(iOS 13.0, *)
extension STGoogleFacebookSignIn: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding{
    
    func setupAppleSignIn(){
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            self.firstName = appleIDCredential.fullName?.givenName ?? ""
            self.lastName = appleIDCredential.fullName?.familyName ?? ""
            let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nil)
            self.firebaseSigin(credential: credential)
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("authorizationController Error: \(error.localizedDescription)")
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return viewCtrl.view.window!
    }
}
