//
//  AppDelegate.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/28/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import GoogleSignIn
import Firebase

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var myOrientation: UIInterfaceOrientationMask = .portrait
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        STLanguageManager.shared.updateCurrentLanguage()
        self.configureIQKeyboard()
        self.setupSocialLogins(application, launchOptions: launchOptions)
        self.navigateToLoginHome()
        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url) || ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      return GIDSignIn.sharedInstance().handle(url) || ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    private func setupSocialLogins(_ application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func configureIQKeyboard(){
        IQKeyboardManager.shared.toolbarTintColor = .black
        IQKeyboardManager.shared.toolbarBarTintColor = .white
        IQKeyboardManager.shared.enable = true
    }
    
    private func navigateToLoginHome(){
        if STUtils.isUserLoggedIn() {
            STUtils.updateHeaders()
        }
        showHomeView()
    }
    
    func showHomeView(){
        UserDefaults.standard.setValue(0, forKey: STConstant.STUserDefaultsKeys.SIDE_MENU_SELECTED_INDEX)
        STMenuOptionManager.sharedInstance.setupRevealCtrl()
        STMenuOptionManager.sharedInstance.slidingPanel.configureRevealView()
        STMenuOptionManager.sharedInstance.slidingPanel.showCenterPanel(animated: false)
        self.window?.rootViewController = STMenuOptionManager.sharedInstance.slidingPanel
        self.window?.makeKeyAndVisible()
    }
    
   func navigateToSocialPages(socialPageUrl: String) {
        guard let url = URL(string:socialPageUrl),UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
    
    func userLogout(){
        STUtils.clearDirectory()
        UserDefaults.standard.set(false, forKey: STConstant.STUserDefaultsKeys.IS_USER_LOGGEDIN)
        UserDefaults.standard.removeObject(forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_TOKEN)
        UserDefaults.standard.removeObject(forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_INFO)
        showHomeView()
    }
    
    func showSign(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "STSignupViewController") as! STSignupViewController
        initialViewController.isFromLogin = true
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
}
