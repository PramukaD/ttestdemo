//
//  STProfileSettingsModel.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/24/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

struct STProfilrInfo {
    var id = ""
    var fName = ""
    var lNama = ""
    var mobileNumber = ""
    var email = ""
    var dob = "DATE_OF_BIRTHDAY".localized
    var DOB = NSDate()
    var gender = 2 // male = 0 , female = 1, none = 2
    var profileImageUrl = URL.init(string: "")
    var profileImageStringUrl = ""
}

class STProfileSettingsModel {

    let settingsMenu = ["BASIC_INFO".localized, "CHANGE_PASSWORD".localized]
    var profileInfo = STProfilrInfo()
    
}

// profile settings

extension STProfileSettingsModel{
    
     func setUserImage(url: String){
        var user = STUtils.getUser()
        user.image = url
        STUtils.saveUser(user: user)
        self.setProfileInfo()
    }
    
    func getUserInfo() -> User{
        var user = User()
        user.firstName = profileInfo.fName
        user.lastName = profileInfo.lNama
        user.mobile = profileInfo.mobileNumber
        user.email = profileInfo.email
        user.dob = (profileInfo.dob == "DATE_OF_BIRTHDAY".localized) ?  "" : profileInfo.dob
        user._id = profileInfo.id
        user.gender = Double(profileInfo.gender)
        return user
    }
    
    func setProfileInfo(){
        let user = STUtils.getUser()
        profileInfo.fName = user.firstName ?? ""
        profileInfo.lNama = user.lastName ?? ""
        profileInfo.mobileNumber = user.mobile ?? ""
        profileInfo.email = user.email ?? ""
        profileInfo.dob = user.dob ?? ""
        profileInfo.id = user._id ?? ""
        profileInfo.profileImageStringUrl = user.image ?? ""
        if user.gender != nil{
            profileInfo.gender = Int(user.gender ?? 2)
        }
    }
    
    func validateProfileInfo() -> (Bool, String) {
        guard !profileInfo.fName.isEmpty else {
            return (false, "PLEASE_ENTER_FIRST_NAME".localized)
        }
        guard !profileInfo.lNama.isEmpty else {
            return (false, "PLEASE_ENTER_LAST_NAME".localized)
        }
//        guard !profileInfo.mobileNumber.isEmpty else {
//            return (false, "Mobile required")
//        }
        guard !profileInfo.email.isEmpty else {
            return (false, "PLEASE_ENTER_EMAIL".localized)
        }
        guard profileInfo.email.trim().isValidEmail() else {
            return (false,  "INVALID_EMAIL".localized)
        }
//        guard profileInfo.dob.trim() != "Date of Birth" else {
//            return (false,  "DOB required")
//        }
        return (true, "")
    }
    
    func saveImage(image: UIImage)  {
        let resizedImage = image.resizeImage(image: image, targetSize: CGSize.init(width: 1200, height: 1200))
        guard let data = resizedImage.pngData() else {
            return
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return
        }
        do {
            self.profileInfo.profileImageUrl = directory.appendingPathComponent("fileName.png")
            try data.write(to: directory.appendingPathComponent("fileName.png")!)
        } catch {
            print(error.localizedDescription)
        }
    }

}

