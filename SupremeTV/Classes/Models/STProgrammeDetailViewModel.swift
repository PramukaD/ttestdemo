//
//  STProgrammeDetailViewModel.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/5/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import Foundation
import DPArrowMenuKit

class STProgrammeDetailViewModel {

    var didSelectProgramme = TvShowModel()
    var seasonsArray = [SeasonModel]()
    var episodesArray = [EpisodeModel]()
    var commentsArray = [ShowCommentModel]()
    var trendingArray = [TvShowModel]()
       
    var seasonsMenuArray = [DPArrowMenuViewModel]()
    var seasonsStringMenuArray = [String]()

    var didSelectSeasonIndex:Int = 0
    var didSelectSeasonTitle: String = "Select season"
    var comment: String = ""
    
    func configureSeasonsEpisodesDataSource(){
        self.configureSeasonsArray()
        self.createSeasonMenuArray()
        self.configureEpisodesArray()
    }
    
    func configureSeasonsArray(){
        if self.seasonsArray.count != 0 { self.seasonsArray.removeAll() }
        self.seasonsArray = self.didSelectProgramme.seasons ?? [SeasonModel]()
    }
    
    func configureDidSelectSeason(index: Int){
        self.didSelectSeasonTitle = self.seasonsStringMenuArray[index]
        self.didSelectSeasonIndex = index
        self.configureEpisodesArray()
    }
    
    func configureEpisodesArray(){
        if self.episodesArray.count != 0 { self.episodesArray.removeAll() }
        if self.seasonsArray.count != 0 {
            let aSeason = self.seasonsArray[self.didSelectSeasonIndex]
            self.episodesArray = aSeason.episodes!
        }
    }
    
    func createSeasonMenuArray(){
        if self.seasonsMenuArray.count != 0 { self.seasonsMenuArray.removeAll() }
        if self.seasonsStringMenuArray.count != 0 { self.seasonsStringMenuArray.removeAll() }
        if self.seasonsArray.count != 0 {
            for aSeason in self.seasonsArray{
                if aSeason.episodes?.count != 0 {
                    var title = ""
                    if let value = aSeason.seasonNo{title =  String(format:"%.0f", value)}
                    let aMenu = DPArrowMenuViewModel(title: "Season \(title)", imageName: "")
                    self.seasonsStringMenuArray.append("Season \(title)")
                    self.seasonsMenuArray.append(aMenu)
                }
            }
            if self.seasonsStringMenuArray.count != 0{
                self.didSelectSeasonTitle = self.seasonsStringMenuArray.first ?? "Select season"
            }
        }
    }
    
    func getNewComment() -> (ShowCommentModel) {
        var aComment = ShowCommentModel()
        aComment.user = User()
        aComment.comment = self.comment
        aComment.user = STUtils.getUser()
        aComment.tvShow = self.didSelectProgramme._id
        return aComment
    }
       
    func validateComment() -> (Bool, String) {
        guard !self.comment.isEmpty else {
            return (false, "Please enter comment.")
        }
        return (true, "")
    }
    
}
