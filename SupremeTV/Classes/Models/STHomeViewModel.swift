//
//  STHomeViewModel.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STHomeViewModel {

    var didSelectMenuItem: Int = 0
    var horizontalMenuArray = [String]()
    var stCatrgoryArray = [CategoryWithShowModel]()
    var horizontalCategoryMenuArray = [CategoryWithShowModel]()
    var tvShowBannersArray = [TvShowModel]()
    var programmesArray = [TvShowModel]()
    var newsArray = [NewsModel]()
    var advertisementUrl:String = ""
    var newsBannerArray = [NewsModel]()
//    var nowShowing = ScheduleModel()
    var bannerSection = 3
    
    var newsBannerCellHeight: CGFloat{
        return (UIDevice.current.screenType == .iPhones_5_5s_5c_SE_4_4S) ? 217.0 : (UIDevice.current.screenType == .iPhones_6_6s_7_8) ? 252.0 : 275.0
    }
    var programmeCellHeight: CGFloat{
        return (UIDevice.current.screenType == .iPhones_5_5s_5c_SE_4_4S) ? 135.0 : 155.0
    }
    var advertisemntCellHeight: CGFloat{
        return (UIDevice.current.screenType == .iPhones_5_5s_5c_SE_4_4S) ? 250.0 : 270.0
    }
}

// MARK: Setup Data source

extension STHomeViewModel{
    
    func setupCategoryProgemmsDataSource(categoryArray: [CategoryWithShowModel]?){
        let catArray = self.getResponseCategoryArray(categoryArray: categoryArray!)
        if self.stCatrgoryArray.count != 0 { self.stCatrgoryArray.removeAll() }
        if self.horizontalMenuArray.count != 0 { self.horizontalMenuArray.removeAll() }
        self.stCatrgoryArray = self.getCategoryArray(data: categoryArray)
        self.horizontalCategoryMenuArray = self.getHorizontalCategoryArray(data: catArray)
        self.horizontalMenuArray = self.getHorizontalMenuArray(data: catArray)
         setBannerSection()
    }
    
    private func setBannerSection(){
        if self.stCatrgoryArray.count > 1 && self.stCatrgoryArray.count < 3{
            self.bannerSection = self.stCatrgoryArray.count - 1
        }
    }
    
    private func getResponseCategoryArray(categoryArray: [CategoryWithShowModel]) -> [CategoryWithShowModel]{
        if categoryArray.isEmpty { return categoryArray }
        var responseCategoryArray = categoryArray
        var teledrama = CategoryWithShowModel()
        if let idx = responseCategoryArray.firstIndex(where: {$0._id == "5dc416902aeb3179a2ab1ccb"}) { // get / remove Teledrama category.
            teledrama = responseCategoryArray[idx]
            responseCategoryArray.remove(at: idx)
            if teledrama.name != nil {
                responseCategoryArray.insert(teledrama, at: 0) // add Teledrama category at 0 index
            }
        }
        if let idx = responseCategoryArray.firstIndex(where: {$0._id == "5df789c0beb4cf448f5503a3"}) { // remove News category.
            responseCategoryArray.remove(at: idx)
        }
        return responseCategoryArray
    }
    
    func setupBanners(bannersArray: [TvShowModel]){
        if self.tvShowBannersArray.count != 0 { self.tvShowBannersArray.removeAll() }
        self.tvShowBannersArray = bannersArray
    }
    
    func setupNewsArray(newsArray: [NewsModel]){
        if self.newsArray.count != 0 { self.newsArray.removeAll() }
        self.newsArray = newsArray
    }
    
    private func getCategoryArray(data: [CategoryWithShowModel]?) -> [CategoryWithShowModel] {
        var homeCate = CategoryWithShowModel()
        homeCate._id = "-100"
        homeCate.name = "Home"
        homeCate._description = "Home Description"
        homeCate.orderNo = -100
        homeCate.activeStatus = 0.0
        homeCate.show = nil
        
        guard let categoryArray = data else { return [homeCate]}
        var countCat = checkProgrammsCount(cArray: categoryArray)
        if countCat.count != 0 {
            countCat.insert(homeCate, at: 0)
            return countCat
        }else{
            countCat.append(homeCate)
            return countCat
        }
    }
    
    private func checkProgrammsCount(cArray: [CategoryWithShowModel]?) ->  [CategoryWithShowModel]{
        var categoryArray = [CategoryWithShowModel]()
        for aCategry in cArray!{
            if aCategry.show!.count >= 5 {
                categoryArray.append(aCategry)
            }
        }
        return categoryArray
    }
    
    private func getHorizontalCategoryArray(data: [CategoryWithShowModel]?) -> [CategoryWithShowModel] {
        var homeCate = CategoryWithShowModel()
        homeCate._id = "-100"
        homeCate.name = "Home"
        homeCate._description = "Home Description"
        homeCate.orderNo = -100
        homeCate.activeStatus = 0.0
        homeCate.show = nil
        var newsCate = CategoryWithShowModel()
        newsCate._id = "-100"
        newsCate.name = "News"
        newsCate._description = "News Description"
        newsCate.orderNo = -100
        newsCate.activeStatus = 0.0
        newsCate.show = nil
        
        guard var categoryArray = data else { return [homeCate, newsCate]}
        if categoryArray.count != 0 {
            categoryArray.insert(newsCate, at: 0)
            categoryArray.insert(homeCate, at: 0)
            return categoryArray
        }
        return [homeCate, newsCate]
    }
    
    private func getHorizontalMenuArray(data: [CategoryWithShowModel]?) -> [String]{
        var menuArray = [String]()
        menuArray.append("Home")
        menuArray.append("Watch Live")
        menuArray.append("News")
        guard let categoryArray = data else { return menuArray}
        if categoryArray.count != 0 {
            for aCategory in categoryArray{
                menuArray.append(aCategory.name ?? "SupremeTV")
            }
            return menuArray
        }
        return menuArray
    }
    
    func setupProgrammesArray(){
        if self.programmesArray.count != 0 { self.programmesArray.removeAll() }
        let aCategory = self.horizontalCategoryMenuArray[(self.didSelectMenuItem - 1)]
        if aCategory.show!.count != 0 { self.programmesArray =  aCategory.show! }
    }
    
    func isNewsView() -> Bool{
        return (self.didSelectMenuItem == 2) ? true : false
    }
}
