//
//  UIImageView+SLCyberMall.swift
//  SLCyberMall
//
//  Created by Persystance Networks on 11/22/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import AlamofireImage
import KingfisherWebP
import Kingfisher

extension UIImageView {
    
    func setupImage(imageUrl: String, imageViewSize: CGSize,placeholderImage: String) {
        self.contentMode = .scaleAspectFill
        if imageUrl.count != 0 {
            let encodedImage = imageUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            let imageurl = URL(string: encodedImage!)
            let filter = AspectScaledToFillSizeFilter(size: (imageViewSize))
            self.af_setImage(withURL: imageurl!, placeholderImage: UIImage.init(named: placeholderImage),filter: filter, runImageTransitionIfCached: true, completion: { (response) in
                if response.result.isSuccess{
                    self.contentMode = .scaleToFill
                }else{
                    let url = URL(string: imageUrl)
                    self.kf.setImage(with: url, options: [.processor(WebPProcessor.default), .cacheSerializer(WebPSerializer.default)])
                    self.contentMode = .scaleToFill
                }
            })
        }else {
            self.image = UIImage.init(named: placeholderImage)
        }
    }
    
}

extension UIImage {
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height *      widthRatio)
        }
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
}
