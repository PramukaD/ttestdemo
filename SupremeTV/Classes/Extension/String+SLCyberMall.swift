//
//  String+SLCyberMall.swift
//  SLCyberMall
//
//  Created by Persystance Networks on 11/26/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

extension String {
    
    func trim() -> String{
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func formattedProgrammeDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formattedDate = dateFormatter.string(from: date!)
        return formattedDate
    }
    
    
    func getTime() -> String {
        if self.isEmpty {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "h:mm a"
        let Date12 = dateFormatter.string(from: date!)
        return Date12
    }
    
    func strikethroughColor() -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.init(hexString: "#7e7e7e"), range: NSMakeRange(0, attributedString.length))
        return attributedString
    }
    
    func getAttributeString(title: String, ftitle: String, color: UIColor, fcolor: UIColor) -> NSAttributedString{
        let attributeString = NSMutableAttributedString(string: "\(title)\(ftitle)");
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: title.count))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#faab19"), range: NSRange(location: title.count, length: ftitle.count))
        return attributeString
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let email = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return email.evaluate(with: self)
    }
}

extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}


/*
 func formattedProgrammeDate() -> String {
     let dateFormatter = DateFormatter()
     let date = self.getDate(dateStr: self)
     dateFormatter.dateFormat = "yyyy-MM-dd"
     let formattedDate = dateFormatter.string(from: date)
     return formattedDate
 }
 
 func getDate(dateStr: String) -> Date{
     let formatters = [
         "yyyy-MM-dd",
         "yyyy-MM-dd'T'HH:mm:ssZZZZZ",
         "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ",
         "yyyy-MM-dd'T'HH:mm:ss'Z'",
         "yyyy-MM-dd'T'HH:mm:ss.SSS",
         "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
         "yyyy-MM-dd HH:mm:ss"
         ].map { (format: String) -> DateFormatter in
             let formatter = DateFormatter()
             formatter.locale = Locale(identifier: "en_US_POSIX")
             formatter.dateFormat = format
             return formatter
     }

     for formatter in formatters {
         if let date = formatter.date(from: dateStr) {
             return date
         }
     }
     return Date()
 }
 */
