//
//  UILabel+SLCyberMall.swift
//  SLCyberMall
//
//  Created by Persystance Networks on 11/26/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setAttributeText(title: String, ftitle: String){
        let att = NSMutableAttributedString(string: "\(title)\(ftitle)");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: title.count))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#faab19"), range: NSRange(location: title.count, length: ftitle.count))
        self.attributedText = att
    }
}

extension UITextField {
    
    func setLeftPaddingPoints(amount: CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func isEmpty() -> Bool{
        return (self.text!.trim().count == 0) ? true : false
    }
    
    var clearButton: UIButton? {
        return value(forKey: "clearButton") as? UIButton
    }

    var clearButtonTintColor: UIColor? {
        get {
            return clearButton?.tintColor
        }
        set {
            let image =  clearButton?.imageView?.image?.withRenderingMode(.alwaysTemplate)
            clearButton?.setImage(image, for: .normal)
            clearButton?.tintColor = .red
        }
    }
    
    func setClearButtonImage() {
        let clearButton : UIButton = self.value(forKey: "_clearButton") as! UIButton
        let image = UIImage(named: "ic_search_close")
        clearButton.setImage(image, for: .normal)
        clearButton.backgroundColor = UIColor.clear
    }
    
}

@IBDesignable class UIAlignedLabel: UILabel {

    override func drawText(in rect: CGRect) {
        if let text = text as NSString? {
            func defaultRect(for maxSize: CGSize) -> CGRect {
                let size = text
                    .boundingRect(
                        with: maxSize,
                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                        attributes: [
                            NSAttributedString.Key.font: font!
                        ],
                        context: nil
                    ).size
                let rect = CGRect(
                    origin: .zero,
                    size: CGSize(
                        width: min(frame.width, ceil(size.width)),
                        height: min(frame.height, ceil(size.height))
                    )
                )
                return rect

            }
            switch contentMode {
            case .top, .bottom, .left, .right, .topLeft, .topRight, .bottomLeft, .bottomRight:
                let maxSize = CGSize(width: frame.width, height: frame.height)
                var rect = defaultRect(for: maxSize)
                switch contentMode {
                    case .bottom, .bottomLeft, .bottomRight:
                        rect.origin.y = frame.height - rect.height
                    default: break
                }
                switch contentMode {
                    case .right, .topRight, .bottomRight:
                        rect.origin.x = frame.width - rect.width
                    default: break
                }
                super.drawText(in: rect)
            default:
                super.drawText(in: rect)
            }
        } else {
            super.drawText(in: rect)
        }
    }

}
