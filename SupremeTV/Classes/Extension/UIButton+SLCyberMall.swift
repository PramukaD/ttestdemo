//
//  UIButton+SLCyberMall.swift
//  SLCyberMall
//
//  Created by Persystance Networks on 11/26/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setButtonGradient(width: CGFloat){
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect.init(x: 0, y: 0, width: width, height: 40.0)
        let topColor: CGColor = UIColor.init(hexString: "#b0751d").cgColor
        let bottomColor: CGColor = UIColor.init(hexString: "#fbc92e").cgColor
        gradientLayer.colors = [topColor, bottomColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.3)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.3)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setAttributeText(title: String, ftitle: String){
        let att = NSMutableAttributedString(string: "\(title)\(ftitle)");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: title.count))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(hexString: "#faab19"), range: NSRange(location: title.count, length: ftitle.count))
        self.setAttributedTitle(att, for: .normal)
    }
}
