//
//  STSearchViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STSearchViewController: STBaseViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var cancelButton: UIButton!

    private var searchedProgrammesArray = [TvShowModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchTextField.becomeFirstResponder()
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text?.trim()
        self.getSearchedProgrammes(serachKey: text!)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: Configure Home View

extension STSearchViewController{
    
    private func configureSearchView(){
        searchCollectionView.isHidden = true
        searchTextField.setLeftPaddingPoints(15.0)
        searchTextField.setClearButtonImage()
        searchTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        searchTextField.placeholder = "SEARC_NEWS_TV_SHOWS_VIDEOS".localized
        configureCollectionView()
    }
    
    private func configureCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 7
        self.searchCollectionView.setCollectionViewLayout(layout, animated: true)
        self.searchCollectionView.register(UINib(nibName: "STHomeProgrammeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STHomeProgrammeCollectionViewCell")
        self.searchCollectionView.dataSource = self
        self.searchCollectionView.delegate = self
    }
}

// MARK: Search API

extension STSearchViewController{
    
    private func getSearchedProgrammes(serachKey: String){
        if !isConnectedToInternet(){ return }
        SupremeTVAPI.getSearchedProgrammes(withSearchKey: serachKey, onSuccess: {(_ programmes: [TvShowModel]) -> Void in
            if self.searchedProgrammesArray.count != 0 { self.searchedProgrammesArray.removeAll() }
            self.searchedProgrammesArray = programmes
            self.searchCollectionView.reloadData()
            self.searchCollectionView.isHidden = (self.searchedProgrammesArray.count != 0) ? false : true
        }, onError: {(_ message) -> Void in })
    }
}

// MARK: Search Collection View UICollectionViewDataSource

extension STSearchViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searchedProgrammesArray.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STHomeProgrammeCollectionViewCell", for: indexPath) as! STHomeProgrammeCollectionViewCell
        cell.setupProgrammeImageViewHeight()
        cell.setupProgrammeDetails(programme: self.searchedProgrammesArray[indexPath.row])
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let aProgramme = self.searchedProgrammesArray[indexPath.row]
        self.navigateToProgrammeDeatilView(aProgramme: aProgramme)
    }
    
}

// MARK: Search Collection View UICollectionViewDelegateFlowLayout

extension STSearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            return CGSize(width:widthPerItem, height:125.0)
        }
        return CGSize(width:widthPerItem, height:145.0)
    }
}
