//
//  STHomeViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/3/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import Parchment

class STHomeViewController: STBaseViewController, STHomeProgrammeTableViewCellDelegate, STHomeBannerTableViewDelegate {

    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var electionImageView: UIImageView!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var homeCollectionView: UICollectionView!

    @IBOutlet weak var revealIcon: UIImageView!
    @IBOutlet weak var supremetvIcon: UIImageView!

    private let homeViewModel = STHomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHomeView()
    }
   
    @IBAction func electionButtonAction(_ sender: Any) {
        let electionResultView = self.storyboard?.instantiateViewController(withIdentifier: "STElectionResultViewController") as! STElectionResultViewController
        self.navigationController?.pushViewController(electionResultView, animated: true)
    }
}

// MARK: Configure Home View

extension STHomeViewController{
    
    private func configureHomeView(){
        configureElectionBanner()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadHomeTable), name: Notification.Name(STConstant.STUserDefaultsKeys.RELOAD_HOME_NOTIFICATION), object: nil)

        self.noDataLabel.isHidden = true
        self.homeTableView.isHidden = true
        self.homeCollectionView.isHidden = true
                 
        self.homeTableView.dataSource = self
        self.homeTableView.delegate = self
        
        self.homeCollectionView.dataSource = self
        self.homeCollectionView.delegate = self
        
        if !isConnectedToInternet(){ return }

        self.getBanners()
//        self.getNowShowing()
        self.getAdvertisement()
        self.getCategories()
        self.getNewsBanner()
        self.getNews(isLoading: false)
    }
    
    private func configureElectionBanner(){
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            electionImageView.contentMode = .scaleAspectFit
            return
        }
    }
    
}

// MARK: API

extension STHomeViewController{
    
    private func getCategories(){
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getCategories(onSuccess: {(_ categoryArray: [CategoryWithShowModel]) -> Void in
            self.stopActicityAnimating()
            self.homeViewModel.setupCategoryProgemmsDataSource(categoryArray: categoryArray)
            self.configureMenu()
            self.homeTableView.reloadData()
            self.homeTableView.isHidden = false
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }
    
    private func getBanners(){
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getHomeBanners(onSuccess: {(_ bannersArray: [TvShowModel]) -> Void in
            self.stopActicityAnimating()
            self.homeViewModel.setupBanners(bannersArray: bannersArray)
            self.homeTableView.reloadData()
            self.homeTableView.isHidden = false
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }

    private func getAdvertisement(){
        SupremeTVAPI.getAdvertisement(onSuccess: {(_ bannerUrl: String) -> Void in
            self.homeViewModel.advertisementUrl = bannerUrl
            self.homeTableView.reloadData()
            self.homeTableView.isHidden = false
        }, onError: {(_ message) -> Void in })
    }
    
//    private func getNowShowing(){
//        SupremeTVAPI.getNowShowing(onSuccess: {(_ nowShowing: ScheduleModel) -> Void in
//            self.homeViewModel.nowShowing = nowShowing
//            self.homeTableView.reloadData()
//            self.homeTableView.isHidden = false
//        }, onError: {(_ message) -> Void in })
//    }
    
    private func getNews(isLoading: Bool){
        if isLoading{ self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT) }
        SupremeTVAPI.fetchNews(onSuccess: {(_ newsArray: [NewsModel]) -> Void in
            self.stopActicityAnimating()
            self.homeViewModel.setupNewsArray(newsArray: newsArray)
            self.homeCollectionView.reloadData()
            self.noDataLabel.isHidden = (self.homeViewModel.newsArray.count == 0) ? false : true
            self.homeCollectionView.isHidden = (self.homeViewModel.newsArray.count != 0) ? false : true
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }

    private func getNewsBanner(){
        SupremeTVAPI.fetchNewsBanners(onSuccess: {(_ newsBannerArray: [NewsModel]) -> Void in
            self.homeViewModel.newsBannerArray = newsBannerArray
        }, onError: {(_ message) -> Void in })
    }
    
}

// MARK: Navigation bar buton actions

extension STHomeViewController{
    
    func didSelectBannerIndex(index: Int){
        guard self.homeViewModel.tvShowBannersArray.count != 0 else {return}
        let show = self.homeViewModel.tvShowBannersArray[index]
        self.navigateToProgrammeDeatilView(aProgramme: show)
    }
    
    @IBAction func revealMenuButtonAction(_ sender: Any) {
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        let searchView = self.storyboard?.instantiateViewController(withIdentifier: "STSearchViewController") as! STSearchViewController
        self.navigationController?.pushViewController(searchView, animated: true)
    }
    

    
    @IBAction func laguageSelectionButtonAction(_ sender: Any) {
        self.displayLanguageSelectionView()
    }
    
//    @objc private func liveButtonAction(){
//        self.navigateToPlayerView(url: STUtils.getPlayerUrl(tvShow: self.homeViewModel.nowShowing.tvShow ?? TvShowModel()))
//        self.homeTableView.reloadData()
//    }
    
    private func navigateToWatchLiveView(){
        let watchLiveView = self.storyboard?.instantiateViewController(withIdentifier: "STWatchLiveViewController") as! STWatchLiveViewController
        self.navigationController?.pushViewController(watchLiveView, animated: true)
    }
    
    @objc private func reloadHomeTable(){
        self.homeTableView.reloadData()
    }
}

// MARK: Set Categories and Programmes

extension STHomeViewController{

    private func configureHomeWatchLive(){
        homeTableView.reloadData()
        homeTableView.isHidden = false
        if self.homeViewModel.programmesArray.count != 0 { self.homeViewModel.programmesArray.removeAll() }
        homeCollectionView.reloadData()
        homeCollectionView.isHidden = true
        self.noDataLabel.isHidden = true
    }
    
    private func configureCollectionViewOtherCategory(){
        self.noDataLabel.isHidden = false
        self.homeViewModel.setupProgrammesArray()
        homeCollectionView.reloadData()
        homeTableView.isHidden = true
        homeCollectionView.isHidden = (self.homeViewModel.programmesArray.count != 0) ? false : true
    }
    
    private func configureNewsCategory(){
        homeTableView.isHidden = true
        if self.homeViewModel.newsArray.count == 0{
            self.getNewsBanner()
            self.getNews(isLoading: true)
        }else{
            homeCollectionView.reloadData()
            noDataLabel.isHidden = (self.homeViewModel.newsArray.count == 0) ? false : true
            homeCollectionView.isHidden = (self.homeViewModel.newsArray.count != 0) ? false : true
        }
    }
       
}

// MARK: Home TableView UITableViewDataSource, UITableViewDelegate

extension STHomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("STHomeTableHeaderView", owner: nil, options: nil)?[0] as? STHomeTableHeaderView
        let aCategory = self.homeViewModel.stCatrgoryArray[section]
        headerView?.setLabelWidth(headerText: (aCategory.name?.localized)!)
        let tap = UITapGestureRecognizer(target:self, action:#selector(self.tableHeaderLabelTapGestureAction(sender:)))
        headerView?.titleLabel.isUserInteractionEnabled = true
        headerView?.titleLabel.addGestureRecognizer(tap)
        tap.view?.tag = section
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { return 0 }
        let aCategory = self.homeViewModel.stCatrgoryArray[section]
        return (aCategory.show!.count == 0) ? 0 : 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.homeViewModel.stCatrgoryArray.count != 0 {
            return self.homeViewModel.stCatrgoryArray.count
        }else{
            return (self.homeViewModel.tvShowBannersArray.count == 0) ? 0 : 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let appDel = UIApplication.shared.delegate as! AppDelegate
        return (appDel.myOrientation == .portrait) ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return configurHomeCell(tableView: tableView, indexPath: indexPath)
        }
        return configurCategoryCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 { return UITableView.automaticDimension }
        let aCategory = self.homeViewModel.stCatrgoryArray[indexPath.section]
        return (aCategory.show!.count == 0) ? 0 : (indexPath.section == homeViewModel.bannerSection) ? self.homeViewModel.advertisemntCellHeight : self.homeViewModel.programmeCellHeight
    }
   
    private func configurHomeCell(tableView: UITableView, indexPath: IndexPath) -> STHomeBannerTableViewCell{
        self.homeTableView.register(UINib(nibName: "STHomeBannerTableViewCell", bundle: nil), forCellReuseIdentifier: "STHomeBannerTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STHomeBannerTableViewCell") as! STHomeBannerTableViewCell
        cell.setupBanerView(tvShowArray: self.homeViewModel.tvShowBannersArray)
        cell.delegate = self
//        cell.liveButton.addTarget(self, action: #selector(self.liveButtonAction), for: .touchUpInside)
        return cell
    }
    
    private func configurCategoryCell(tableView: UITableView, indexPath: IndexPath) -> STHomeProgrammeTableViewCell{
        self.homeTableView.register(UINib(nibName: "STHomeProgrammeTableViewCell", bundle: nil), forCellReuseIdentifier: "STHomeProgrammeTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STHomeProgrammeTableViewCell") as! STHomeProgrammeTableViewCell
        let aCategory = self.homeViewModel.stCatrgoryArray[indexPath.section]
        cell.programmeArray = aCategory.show!
        cell.delegate = self
        cell.reloadProgrammeCollectionView(programmes:aCategory.show!, section: indexPath.section, bannerSection: homeViewModel.bannerSection)
        if indexPath.section == homeViewModel.bannerSection { cell.setupBannerView(imageUrl: self.homeViewModel.advertisementUrl)}
        return cell
    }
}

// MARK: Did select Programme / Header

extension STHomeViewController{
    
    func didSelectProgrammeIndexPath(indexPath: IndexPath, section: Int){
        let aCategory = self.homeViewModel.stCatrgoryArray[section]
        let aProgramme = aCategory.show![indexPath.row] // as? TvShowModel
        self.navigateToProgrammeDeatilView(aProgramme: aProgramme)
    }
    
    @objc private func tableHeaderLabelTapGestureAction(sender:UITapGestureRecognizer) {
        let tag = sender.view!.tag
        let aCategory = self.homeViewModel.stCatrgoryArray[tag]
        print("Category ID: \(aCategory._id!) ====  Category Name: \(aCategory.name!)")
    }
    
}

// MARK: Home Collection View UICollectionViewDataSource

extension STHomeViewController: UICollectionViewDataSource {
    
    // self.homeViewModel.didSelectMenuItem
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.homeViewModel.isNewsView() {
            return 2
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.homeViewModel.isNewsView() {
            if section == 0 {
                return (self.homeViewModel.newsBannerArray.isEmpty) ? 0 : 1
            }
            return self.homeViewModel.newsArray.count
        }
        return self.homeViewModel.programmesArray.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.homeViewModel.isNewsView() {
            if indexPath.section == 0 {
                return self.configurNewsBannerCell(collectionView: collectionView, indexPath: indexPath)
            }
            return self.configurNewsCell(collectionView: collectionView, indexPath: indexPath)
        }
        return self.configurProgrammsCell(collectionView: collectionView, indexPath: indexPath)
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if !self.homeViewModel.isNewsView(){
            let aProgramme = self.homeViewModel.programmesArray[indexPath.row]
            self.navigateToProgrammeDeatilView(aProgramme: aProgramme)
            return
        }
        if indexPath.section == 1 {
            let aNews = self.homeViewModel.newsArray[indexPath.row]
            self.navigateToPlayerView(url: aNews.url ?? "")
        }
    }
    
    func didSelectNewsBannerIndex(index: Int){
        let aNews = self.homeViewModel.newsBannerArray[index]
        self.navigateToPlayerView(url: aNews.url ?? "")
    }
    
    private func configurProgrammsCell(collectionView: UICollectionView, indexPath: IndexPath) -> STHomeProgrammeCollectionViewCell{
        self.homeCollectionView.register(UINib(nibName: "STHomeProgrammeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STHomeProgrammeCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STHomeProgrammeCollectionViewCell", for: indexPath) as! STHomeProgrammeCollectionViewCell
        cell.setupProgrammeImageViewHeight()
        cell.setupProgrammeDetails(programme: self.homeViewModel.programmesArray[indexPath.row])
        return cell
    }
    
    private func configurNewsBannerCell(collectionView: UICollectionView, indexPath: IndexPath) -> STNewsBannerCollectionViewCell{
        self.homeCollectionView.register(UINib(nibName: "STNewsBannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STNewsBannerCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STNewsBannerCollectionViewCell", for: indexPath) as! STNewsBannerCollectionViewCell
        cell.setupBanerView(bannerArray: self.homeViewModel.newsBannerArray)
        return cell
    }
    
    private func configurNewsCell(collectionView: UICollectionView, indexPath: IndexPath) -> STNewsCollectionViewCell{
        self.homeCollectionView.register(UINib(nibName: "STNewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STNewsCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STNewsCollectionViewCell", for: indexPath) as! STNewsCollectionViewCell
        cell.setupProgrammeImageViewHeight()
        cell.setupNewsDetails(aNews: self.homeViewModel.newsArray[indexPath.row])
        return cell
    }
}

// MARK: Home Collection View UICollectionViewDelegateFlowLayout

extension STHomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.homeViewModel.isNewsView() {
            if indexPath.section == 0 {
                return CGSize(width:collectionView.frame.width , height:self.homeViewModel.newsBannerCellHeight)
            }
        }
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            return CGSize(width:widthPerItem, height:125.0)
        }
        return CGSize(width:widthPerItem, height:145.0)
    }
}

// MARK: Configure horizontal menus

extension STHomeViewController{

    private func configureMenu() {
        
        let pagingViewController = PagingViewController<PagingIndexItem>()
        pagingViewController.dataSource = self
        pagingViewController.delegate = self
        pagingViewController.view.backgroundColor = .clear
        pagingViewController.collectionView.backgroundColor = .black
        pagingViewController.indicatorColor = UIColor.init(hexString: "#faab19")
        pagingViewController.borderOptions = .hidden
        pagingViewController.font = UIFont.boldSystemFont(ofSize: 15.0)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15.0)
        pagingViewController.textColor = UIColor.init(hexString: "#ababab")
        pagingViewController.selectedTextColor = UIColor.init(hexString: "#faab19")
        addChild(pagingViewController)
        containerView.addSubview(pagingViewController.view)
        containerView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
}

// MARK: PagingViewControllerDelegate, PagingViewControllerDataSource

extension STHomeViewController: PagingViewControllerDataSource, PagingViewControllerDelegate {
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T {
        print( self.homeViewModel.horizontalMenuArray[index])
        return PagingIndexItem(index: index, title: self.homeViewModel.horizontalMenuArray[index].localized.uppercased()) as! T
    }
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController {
        return UIViewController()
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, didScrollToItem pagingItem: T, startingViewController: UIViewController?, destinationViewController: UIViewController,transitionSuccessful: Bool){
        view.endEditing(true)
        let item = pagingItem as! PagingIndexItem
        self.homeViewModel.didSelectMenuItem = item.index
        if item.index == 0 {
            self.configureHomeWatchLive()
        }else if item.index == 1 {
            self.configureHomeWatchLive()
            navigateToWatchLiveView()
        }else if item.index == 2 {
            self.configureNewsCategory()
        }else{
            self.configureCollectionViewOtherCategory()
        }
    }
    
    func numberOfViewControllers<T>(in: PagingViewController<T>) -> Int {
        return self.homeViewModel.horizontalMenuArray.count
    }
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, widthForPagingItem pagingItem: T, isSelected: Bool) -> CGFloat? {
        guard let item = pagingItem as? PagingIndexItem else { return 0 }
        let insets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: pagingViewController.menuItemSize.height)
        let attributes = [NSAttributedString.Key.font: pagingViewController.font]
        let rect = item.title.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes,context: nil)
        let width = ceil(rect.width) + insets.left + insets.right
        return width
    }
     
}
