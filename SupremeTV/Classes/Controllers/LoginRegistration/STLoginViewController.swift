//
//  STLoginViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/28/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import GoogleSignIn

enum STSocialLogin {
    case facebook
    case google
    case apple
}

class STLoginViewController: STBaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var gPlusButton: UIButton!
    @IBOutlet weak var backRevealButton: UIButton!
    @IBOutlet weak var appleSiginView: UIView!

    @IBOutlet weak var revealIcon: UIImageView!
    @IBOutlet weak var backIcon: UIImageView!

    @IBOutlet weak var appleSiginViewHeight: NSLayoutConstraint!
    @IBOutlet weak var forgotButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var navigationViewTopSpace: NSLayoutConstraint!
    @IBOutlet weak var buttonBottomSpace: NSLayoutConstraint!
    @IBOutlet var socialButtonHeight: [NSLayoutConstraint]!
    @IBOutlet var socialButtonWidth: [NSLayoutConstraint]!
    
    let googleFacebookSignin = STGoogleFacebookSignIn()
    var isHiddenBackButtonView: Bool = true
    
    private var loginMethod :STSocialLogin = .google
    var isFromSignUp: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLoginView()
        
//        self.emailTextField.text = "iospn@gmail.com"
//        self.passwordTextField.text = "123456"

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        configureLoginViewWillLayoutSubviews()
    }
}

// MARK: Configure views

extension STLoginViewController{
    
    private func configureLoginView(){
        configureGoogleSignin()
        emailTextField.setLeftPaddingPoints(15.0)
        passwordTextField.setLeftPaddingPoints(15.0)
        forgotButton.contentHorizontalAlignment = .right
        signupButton.setAttributeText(title: "DO_NOT_HAVE_AN_ACCOUNT".localized, ftitle: "SIGN_UP_SPACE".localized)
        if self.isFromSignUp{
            self.revealIcon.isHidden = true
            self.backIcon.isHidden = false
            self.backRevealButton.addTarget(self, action: #selector(self.backButtonAction), for: .touchUpInside)
        }else{
            self.revealIcon.isHidden = false
            self.backIcon.isHidden = true
            self.backRevealButton.addTarget(self, action: #selector(self.revealMenuButtonAction), for: .touchUpInside)
        }
        emailTextField.placeholder = "\("EMAIL".localized) \("OR".localized) \("MOBILE_NUMBER".localized)"
        passwordTextField.placeholder = "PASSWORD".localized
        if #available(iOS 13.0, *) { appleSiginView.isHidden = false
        } else { appleSiginView.isHidden = true}
    }
    
    private func configureLoginViewWillLayoutSubviews(){
        continueButton.setButtonGradient(width:view.bounds.width - 40)
        continueButton.clipsToBounds = true
        if #available(iOS 13.0, *) { appleSiginViewHeight.constant = 35.0
        } else {appleSiginViewHeight.constant = 0}
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{return}
        navigationViewTopSpace.constant = 30.0
        buttonBottomSpace.constant = 35.0
        for aConstraint in socialButtonWidth{ aConstraint.constant = 140.0 }
        for aConstraint in socialButtonHeight{ aConstraint.constant = 35.0 }
    }
}

// MARK: Button actions
extension STLoginViewController{
    
    @objc private func revealMenuButtonAction() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showHomeView()
    }
    
    @objc private func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func facebookButtonAction(_ sender: Any) {
        if !isConnectedToInternet(){ return }
        self.loginMethod = .facebook
        LoginManager().logOut()
        self.googleFacebookSignin.doFacebookLogin()
    }
           
    @IBAction func gPlusButtonAction(_ sender: Any) {
        if !isConnectedToInternet(){ return }
        self.loginMethod = .google // "1"
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func forgotButtonAction(_ sender: Any) {
        let forgotView = self.storyboard?.instantiateViewController(withIdentifier: "STForgotViewController") as! STForgotViewController
        self.navigationController?.pushViewController(forgotView, animated: true)
    }
       
    @IBAction func signupButtonAction(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: STSignupViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                return
            }
        }
        let signupView = self.storyboard?.instantiateViewController(withIdentifier: "STSignupViewController") as! STSignupViewController
        signupView.isFromLogin = true
        self.navigationController?.pushViewController(signupView, animated: true)
    }
       
    @IBAction func continueButtonAction(_ sender: Any) {
        let (validationStatus, message) = self.validateUserLogin()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        if !isConnectedToInternet(){ return }
       doLogin()
    }
    
    @available(iOS 13.0, *)
    @IBAction func appleSigninButtonAction(_ sender: Any) {
        if !isConnectedToInternet(){ return }
        self.loginMethod = .apple
        googleFacebookSignin.setupAppleSignIn()
    }
}

// MARK: Login API

extension STLoginViewController{
    
    private func doLogin(){
        var user = User()
        user.username = self.emailTextField.text?.trim()
        user.password = self.passwordTextField.text?.trim()
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userLogin(loginData: user, onSuccess: {(_ user: User) -> Void in
            self.stopActicityAnimating()
            STUtils.userNavigation(user: user)
        }, onError: {(_ message) -> Void in
            STUtils.displayAlert(message: message, themeStyle: .error)
            self.stopActicityAnimating()
        })
    }
    
    private func userOauthLogin(token: String){
        var oauth = OauthLogin()
        oauth.loginMethod = (self.loginMethod == .google) ? "1" : "2"
        oauth.token = token
        if loginMethod == .apple{
            oauth.loginMethod = "3"
            oauth.firstName = googleFacebookSignin.firstName
            oauth.lastName = googleFacebookSignin.lastName
        }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userOauthLogin(loginData: oauth, onSuccess: {(_ data: User) -> Void in
            self.stopActicityAnimating()
            STUtils.userNavigation(user: data)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
}

// MARK: Google, Facebook Signin

extension STLoginViewController: STGoogleFacebookSignInDelegate {
    
    private func configureGoogleSignin(){
        googleFacebookSignin.viewCtrl = self
        googleFacebookSignin.delegate = self
        GIDSignIn.sharedInstance().delegate = googleFacebookSignin as GIDSignInDelegate
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    
    func socialLoginToken(idToken: String){
        self.userOauthLogin(token: idToken)
    }
    
}

// MARK: Validate user login credentials 

extension STLoginViewController{
    
    private func validateUserLogin() -> (Bool, String) {
        guard !self.emailTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_EMAIL_OR_MOBILE".localized)
        }
        guard !self.passwordTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_PASSWORD".localized)
        }
        return (true, "")
    }
    
}
