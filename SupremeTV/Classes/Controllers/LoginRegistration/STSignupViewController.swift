//
//  STSignupViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/29/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn

class STSignupViewController: STBaseViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    @IBOutlet weak var logintButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var gPlusButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var backRevealButton: UIButton!
    @IBOutlet weak var appleSiginView: UIView!

    @IBOutlet weak var revealIcon: UIImageView!
    @IBOutlet weak var backIcon: UIImageView!
    
    @IBOutlet weak var appleSiginViewHeight: NSLayoutConstraint!
    @IBOutlet var viewsSpace: [NSLayoutConstraint]!
    @IBOutlet var bottomSpace: [NSLayoutConstraint]!
    @IBOutlet var socialButtonHeight: [NSLayoutConstraint]!
    @IBOutlet var socialButtonWidth: [NSLayoutConstraint]!

    private var signupMethod :STSocialLogin = .google
    var isFromLogin: Bool = false

    let googleFacebookSignin = STGoogleFacebookSignIn()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSignupView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        configureSignupViewWillLayoutSubviews()
    }

}

// MARK: Configure Signup View

extension STSignupViewController{
    
    private func configureSignupView(){
        configureGoogleSignin()
        userNameTextField.setLeftPaddingPoints(15.0)
        emailTextField.setLeftPaddingPoints(15.0)
        passwordTextField.setLeftPaddingPoints(15.0)
        logintButton.setAttributeText(title: "ALREADY_MEMBER_OF_SUPREMETV".localized, ftitle: "LOGIN_SPACE".localized)
        if self.isFromLogin{
            self.revealIcon.isHidden = true
            self.backIcon.isHidden = false
            self.backRevealButton.addTarget(self, action: #selector(self.backButtonAction), for: .touchUpInside)
        }else{
            self.revealIcon.isHidden = false
            self.backIcon.isHidden = true
            self.backRevealButton.addTarget(self, action: #selector(self.revealMenuButtonAction), for: .touchUpInside)
        }
        userNameTextField.placeholder = "FIRST_NAME".localized
        emailTextField.placeholder = "EMAIL".localized
        passwordTextField.placeholder = "PASSWORD".localized
        if #available(iOS 13.0, *) { appleSiginView.isHidden = false
        } else { appleSiginView.isHidden = true}
    }
    
    private func configureSignupViewWillLayoutSubviews(){
        continueButton.setButtonGradient(width:view.bounds.width - 40)
        continueButton.clipsToBounds = true
        if #available(iOS 13.0, *) { appleSiginViewHeight.constant = 35.0
        } else {appleSiginViewHeight.constant = 0}
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{return}
        for aConstraint in bottomSpace{ aConstraint.constant = 20.0 }
        for aConstraint in viewsSpace{ aConstraint.constant = 15.0 }
        for aConstraint in socialButtonWidth{ aConstraint.constant = 140.0 }
        for aConstraint in socialButtonHeight{ aConstraint.constant = 35.0 }
    }
}

// MARK: Button actions

extension STSignupViewController{
    
    @objc private func revealMenuButtonAction() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showHomeView()
    }
       
    @objc private func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func facebookButtonAction(_ sender: Any) {
        if !isConnectedToInternet(){ return }
        self.signupMethod = .facebook
        self.googleFacebookSignin.doFacebookLogin()
    }
    
    @IBAction func gPlusButtonAction(_ sender: Any) {
        if !isConnectedToInternet(){ return }
        self.signupMethod = .google
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func continueButtonAction(_ sender: Any) {
        let (validationStatus, message) = self.validateUserInfo()
         guard validationStatus else {
             STUtils.displayAlert(message: message, themeStyle: .error)
             return
         }
        if !isConnectedToInternet(){ return }
        self.userRegister()
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: STLoginViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                return
            }
        }
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "STLoginViewController") as! STLoginViewController
        loginView.isFromSignUp = true
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
    @available(iOS 13.0, *)
    @IBAction func appleSigninButtonAction(_ sender: Any) {
       if !isConnectedToInternet(){ return }
       self.signupMethod = .apple
       googleFacebookSignin.setupAppleSignIn()
    }
}

// MARK: API

extension STSignupViewController{
    

    private func userRegister(){
        var user = User()
        user.firstName = self.userNameTextField.text?.trim()
        user.email = self.emailTextField.text?.trim()
        user.password = self.passwordTextField.text?.trim()
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userRegister(registerData: user, onSuccess: {(_ data: User) -> Void in
            self.stopActicityAnimating()
            STUtils.userNavigation(user: data)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
    
    private func userOauthLogin(token: String){
        var oauth = OauthLogin()
        oauth.loginMethod = (self.signupMethod == .google) ? "1" : "2"
        oauth.token = token
        if signupMethod == .apple{
            oauth.loginMethod = "3"
            oauth.firstName = googleFacebookSignin.firstName
            oauth.lastName = googleFacebookSignin.lastName
        }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userOauthLogin(loginData: oauth, onSuccess: {(_ data: User) -> Void in
            self.stopActicityAnimating()
            STUtils.userNavigation(user: data)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
    
}

// MARK: Google, Facebook Signin

extension STSignupViewController: STGoogleFacebookSignInDelegate {
    
    private func configureGoogleSignin(){
        googleFacebookSignin.viewCtrl = self
        googleFacebookSignin.delegate = self
        GIDSignIn.sharedInstance().delegate = googleFacebookSignin
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    
    func socialLoginToken(idToken: String){
        self.userOauthLogin(token: idToken)
    }
}

// MARK: Facebook Signin

extension STSignupViewController {
    
    
}

// MARK: Validate User Info

extension STSignupViewController{
    
    private func validateUserInfo() -> (Bool, String) {
        guard !self.userNameTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_USERNAME".localized)
        }
        guard !self.emailTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_EMAIL".localized)
        }
        guard self.emailTextField.text!.trim().isValidEmail() else {
            return (false,  "INVALID_EMAIL".localized)
        }
        guard !self.passwordTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_PASSWORD".localized)
        }
        return (true, "")
    }
    
}

