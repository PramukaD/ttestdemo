//
//  STProfileSettingsViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/29/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import Parchment
import Photos

class STProfileSettingsViewController: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var passwordsContainerView: UIView!
    @IBOutlet weak var profileSettingTableView: UITableView!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var newPwdTextField: UITextField!
    @IBOutlet weak var retypePwdTextField: UITextField!
    @IBOutlet weak var updatePwdButton: UIButton!
    
    private var imagePicker = UIImagePickerController()

    private let profileSettingsModel = STProfileSettingsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMenu()
        configurePasswordSettingsView()
        configureProfileSettingsView()
        self.navigationController!.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.isNavigationBarHidden = true
    }
    
}

// MARK: Configure Profile Settings View

extension STProfileSettingsViewController{
    
    private func configureProfileSettingsView(){
        self.profileSettingsModel.setProfileInfo()
        passwordsContainerView.isHidden = true
        self.profileSettingTableView.register(UINib(nibName: "STBasicInfoSettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "STBasicInfoSettingsTableViewCell")
        self.profileSettingTableView.dataSource = self
        self.profileSettingTableView.delegate = self
    }
    
    private func configurePasswordSettingsView(){
        pwdTextField.setLeftPaddingPoints(15.0)
        newPwdTextField.setLeftPaddingPoints(15.0)
        retypePwdTextField.setLeftPaddingPoints(15.0)
        updatePwdButton.setButtonGradient(width:view.bounds.width - 40)
        updatePwdButton.clipsToBounds = true
        
        pwdTextField.placeholder = "OLD_PASSWORD".localized
        newPwdTextField.placeholder = "NEW_PASSWORD".localized
        retypePwdTextField.placeholder = "CONFIRM_NEW_PASSWORD".localized
    }
    
    @IBAction func updatePwdButtonAction(_ sender: Any) {
        let (validationStatus, message) = self.validatePasswords()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        self.updatePassword()
    }
    
    @IBAction func pwdShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.pwdTextField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func newPwdShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.newPwdTextField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func retypePwdShowPassword(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.retypePwdTextField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func revealMenuButtonAction(_ sender: Any) {
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }
    
    @IBAction func laguageSelectionButtonAction(_ sender: Any) {
        self.displayLanguageSelectionView()
    }
    
    func updateUserProfile(){
        let cell = self.profileSettingTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! STBasicInfoSettingsTableViewCell?
        cell?.setupProfileCell(profileInfo: self.profileSettingsModel.profileInfo)
    }
    
    func resetPasswordTextFields(){
        self.pwdTextField.text = ""
        self.newPwdTextField.text = ""
        self.retypePwdTextField.text = ""
    }
}


// MARK: API

extension STProfileSettingsViewController{
    
    private func uploadUserAvatar(){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.uploadUserAvatar(image: self.profileSettingsModel.profileInfo.profileImageUrl!, onSuccess: {(_ data: CommonSuccess) -> Void in
            self.stopActicityAnimating()
            self.profileSettingsModel.setUserImage(url: data.data ?? "")
            self.profileSettingsModel.setProfileInfo()
            NotificationCenter.default.post(name: Notification.Name(STConstant.STUserDefaultsKeys.RELOAD_SIDE_MENU_NOTIFICATION), object: nil)
            self.updateUserProfile()
            STUtils.displayAlert(message: "PROFILE_PICTURE_UPDATED_SUCCESSFULLY".localized, themeStyle: .success)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
    
    private func updateProfile(){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.updateProfile(updateDate: self.profileSettingsModel.getUserInfo(), onSuccess: {(_ data: User) -> Void in
            self.stopActicityAnimating()
            STUtils.saveUser(user: data)
            self.profileSettingsModel.setProfileInfo()
            NotificationCenter.default.post(name: Notification.Name(STConstant.STUserDefaultsKeys.RELOAD_SIDE_MENU_NOTIFICATION), object: nil)
            STUtils.clearDirectory()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showHomeView()
            STUtils.displayAlert(message: "PROFILE_UPDATED_SUCCESSFULLY".localized, themeStyle: .success)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }

    private func updatePassword(){
        if !isConnectedToInternet(){ return }
        let aPassword = ChangePassword.init(currentPassword: self.pwdTextField.text?.trim(), newPassword: self.retypePwdTextField.text?.trim())
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.updatePassword(updateData: aPassword, onSuccess: {(_ data: User) -> Void in
            self.stopActicityAnimating()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showHomeView()
            STUtils.displayAlert(message: "PASSWORD_UPDATED_SUCCESSFULLY".localized, themeStyle: .success)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
}

extension STProfileSettingsViewController{
    
    private func validatePasswords() -> (Bool, String) {
        guard !self.pwdTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_CURRENT_PASSWORD".localized)
        }
        guard !self.newPwdTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_NEW_PASSWORD".localized)
        }
        guard !self.retypePwdTextField.isEmpty() else {
            return (false, "PLEASE_RETYPE_PASSWORD".localized)
        }
        guard self.newPwdTextField.text?.trim() ==  self.retypePwdTextField.text?.trim() else {
            return (false, "CURRENT_AND_NEW_PASSWORD_DONT_MATCH".localized)
        }
        return (true, "")
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension STProfileSettingsViewController: UITableViewDelegate, UITableViewDataSource{
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "STBasicInfoSettingsTableViewCell") as! STBasicInfoSettingsTableViewCell
        cellButtonActions(cell: cell)
        textFieldValueChange(cell: cell)
        cell.delegate = self
        cell.setupProfileCell(profileInfo: self.profileSettingsModel.profileInfo)
        cell.saveButton.addTarget(self, action: #selector(self.updateProfileButtonAction), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 625.0
    }
}

// MARK: Setup Cell button actions, text field, TextField Value change, STBasicInfoTableViewCellDelegate

extension STProfileSettingsViewController: STBasicInfoTableViewCellDelegate{
    
    private func cellButtonActions(cell: STBasicInfoSettingsTableViewCell){
        cell.saveButton.addTarget(self, action: #selector(self.updateProfileButtonAction), for: .touchUpInside)
        cell.dobButton.addTarget(self, action: #selector(self.dobButtonAction), for: .touchUpInside)
        cell.photoButton.addTarget(self, action: #selector(self.showActionSheet), for: .touchUpInside)
    }
    
    private func textFieldValueChange(cell: STBasicInfoSettingsTableViewCell){
        cell.fnameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        cell.lnameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        cell.mobileTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        cell.emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        switch textField.tag {
        case 20:
            self.profileSettingsModel.profileInfo.fName = textField.text!
            break
        case 21:
            self.profileSettingsModel.profileInfo.lNama = textField.text!
            break
        case 22:
            self.profileSettingsModel.profileInfo.mobileNumber = textField.text!
            break
        case 23:
            self.profileSettingsModel.profileInfo.email = textField.text!
            break
        default:
            break
        }
    }
    
    @objc func updateProfileButtonAction() {
        let (validationStatus, message) = self.profileSettingsModel.validateProfileInfo()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        self.updateProfile()
    }
    
    @objc func dobButtonAction() {
        DPPickerManager.shared.showPicker(title: "DATE_OF_BIRTHDAY".localized, selected: self.profileSettingsModel.profileInfo.DOB as Date, completion: {(date, cancel) in
            if !cancel {
                self.profileSettingsModel.profileInfo.DOB = date! as NSDate
                self.profileSettingsModel.profileInfo.dob = STUtils.getFormattedDate(date: self.profileSettingsModel.profileInfo.DOB as Date)
                self.updateUserProfile()
            }
         })
    }
    
    func didSelectGender(gender: Bool){
        self.profileSettingsModel.profileInfo.gender = (gender) ? 0 : 1
    }
}

// MARK: Profile Image selection

extension STProfileSettingsViewController {
    
    @objc func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "CAMERA".localized, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                STUtils.displayAlert(message: STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE, themeStyle: .error)
                return
            }
            self.configureImagePicker(sourceType: .camera)
        }))
        actionSheet.addAction(UIAlertAction(title: "GALLERY".localized, style: .default, handler: { (alert:UIAlertAction!) -> Void in
            guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
                STUtils.displayAlert(message: STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE, themeStyle: .error)
                return
            }
            self.configureImagePicker(sourceType: .photoLibrary)
        }))
        actionSheet.addAction(UIAlertAction(title: "CANCEL".localized, style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    private func configureImagePicker(sourceType: UIImagePickerController.SourceType){
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    }
}

// MARK: UIImagePickerControllerDelegate

extension STProfileSettingsViewController{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        picker.dismiss(animated: true, completion: nil)
        guard let originalImage = info[.originalImage] as? UIImage else {
            STUtils.displayAlert(message: STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE, themeStyle: .error)
            return
        }
        self.profileSettingsModel.saveImage(image: originalImage)
        uploadUserAvatar()
    }
    
}

// MARK: Configure menu views

extension STProfileSettingsViewController{

    private func configureMenu() {
        
        let pagingViewController = PagingViewController<PagingIndexItem>()
        pagingViewController.dataSource = self
        pagingViewController.delegate = self
        pagingViewController.view.backgroundColor = .clear
        pagingViewController.collectionView.backgroundColor = .black
        pagingViewController.indicatorColor = UIColor.init(hexString: "#faab19")
        pagingViewController.borderOptions = .hidden
        pagingViewController.font = UIFont.boldSystemFont(ofSize: 15.0)
        pagingViewController.selectedFont = UIFont.boldSystemFont(ofSize: 15.0)
        pagingViewController.textColor = UIColor.init(hexString: "#ababab")
        pagingViewController.selectedTextColor = .white
        addChild(pagingViewController)
        containerView.addSubview(pagingViewController.view)
        containerView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
    }
}

// MARK: PagingViewControllerDelegate, PagingViewControllerDataSource

extension STProfileSettingsViewController: PagingViewControllerDataSource, PagingViewControllerDelegate {
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T {
        return PagingIndexItem(index: index, title: self.profileSettingsModel.settingsMenu[index]) as! T
    }
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController {
        return UIViewController()
    }
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, didScrollToItem pagingItem: T, startingViewController: UIViewController?, destinationViewController: UIViewController,transitionSuccessful: Bool){
        view.endEditing(true)
        let item = pagingItem as! PagingIndexItem
        if item.index == 1{
            passwordsContainerView.isHidden = false
            profileSettingTableView.isHidden = true
        }else{
            passwordsContainerView.isHidden = true
            profileSettingTableView.isHidden = false
        }
    }
    
    func numberOfViewControllers<T>(in: PagingViewController<T>) -> Int {
        let loginType = STUtils.getLoginType()
        if loginType == 1{
            return self.profileSettingsModel.settingsMenu.count
        }
        return 1
    }
  
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, widthForPagingItem pagingItem: T, isSelected: Bool) -> CGFloat? {
        guard let item = pagingItem as? PagingIndexItem else { return 0 }
        let insets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: pagingViewController.menuItemSize.height)
        let attributes = [NSAttributedString.Key.font: pagingViewController.font]
        let rect = item.title.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes,context: nil)
        let width = ceil(rect.width) + insets.left + insets.right
        return width
    }
     
}
