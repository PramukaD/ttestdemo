//
//  STForgotViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/29/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STForgotViewController: STBaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureForgotView()
        // Do any additional setup after loading the view.
    }
    
}

// MARK: Configure views

extension STForgotViewController{
    
    private func configureForgotView(){
        emailTextField.setLeftPaddingPoints(15.0)
        submitButton.setButtonGradient(width:view.bounds.width - 40)
        submitButton.clipsToBounds = true
        emailTextField.placeholder = "EMAIL".localized
    }
    
}

// MARK: Button actions

extension STForgotViewController{
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        let (validationStatus, message) = self.validateEmail()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        forgotPassword()
    }
}

// MARK: API

extension STForgotViewController{
    
    private func forgotPassword(){
        if !isConnectedToInternet(){ return }
        var user = User()
        user.username = self.emailTextField.text?.trim()
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userForgotPassword(data: user, onSuccess: {(_ data: CommonSuccess) -> Void in
            self.stopActicityAnimating()
            let message = "FORGOT_PASWORD_MESSAGE".localized + " \(self.emailTextField.text?.trim() ?? "")"
            STUtils.displayAlert(message: message, themeStyle: .success)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }
}


// MARK: Validate Email

extension STForgotViewController{
    
    private func validateEmail() -> (Bool, String) {
        guard !self.emailTextField.isEmpty() else {
            return (false, "PLEASE_ENTER_EMAIL".localized)
        }
        guard self.emailTextField.text!.trim().isValidEmail() else {
            return (false,  "INVALID_EMAIL".localized)
        }
        return (true, "")
    }
    
}
