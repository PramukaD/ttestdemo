//
//  STElectionResultViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 8/6/20.
//  Copyright © 2020 Persystance Networks. All rights reserved.
//

import UIKit
import WebKit

class STElectionResultViewController: STBaseViewController, WKNavigationDelegate {

    @IBOutlet weak var containerView: UIView!

    private var webView = WKWebView(frame: .zero)
    private var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func loadWebView(){
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.containerView.addSubview(self.webView)
        NSLayoutConstraint.activate([
                    self.webView.leftAnchor.constraint(equalTo: self.containerView.leftAnchor),
                    self.webView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor),
                    self.webView.rightAnchor.constraint(equalTo: self.containerView.rightAnchor),
                    self.webView.topAnchor.constraint(equalTo: self.containerView.topAnchor),
                    ])
        self.containerView.setNeedsLayout()
        
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        activityIndicator.color = UIColor.init(hexString: "#333333")
        view.addSubview(activityIndicator)
        
        let url = URL(string: "https://www.supremetv.lk/election_news/election2020/mlayout.html#!/index")
        let request = URLRequest(url: url! as URL)
        webView.navigationDelegate = self
        showActivityIndicator(show: true)
        webView.load(request)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        showActivityIndicator(show: false)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showActivityIndicator(show: false)
    }
    
    private func showActivityIndicator(show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}
