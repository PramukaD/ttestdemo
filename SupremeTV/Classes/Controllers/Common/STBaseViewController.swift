//
//  STBaseViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/28/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire



class STBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIViewController.attemptRotationToDeviceOrientation()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}



// MARK: Customize navigation bar

extension STBaseViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool){
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.isTranslucent = true
        navigationController.isNavigationBarHidden = true
    }
   
}

extension UIViewController: NVActivityIndicatorViewable {
    
    func displayLanguageSelectionView(){
        let languageSelectionView = self.storyboard?.instantiateViewController(withIdentifier: "STLanguageSelectionViewController") as! STLanguageSelectionViewController
        self.present(languageSelectionView, animated: true, completion: nil)
    }
    
    func navigateToProgrammeDeatilView(aProgramme: TvShowModel){
        let programmeDetailView = self.storyboard?.instantiateViewController(withIdentifier: "STProgrammeDetailViewController") as! STProgrammeDetailViewController
        programmeDetailView.programmeDetailViewModel.didSelectProgramme = aProgramme
        self.navigationController?.pushViewController(programmeDetailView, animated: true)
    }
       
    func navigateToPlayerView(url: String){
        guard STUtils.isYoutubeUrl(youtubeUrl: url) else {
            STUtils.displayAlert(message: "CAN_NOT_PLAY_THIS_VIDEO".localized, themeStyle: .error)
            return
        }
        let playerView = self.storyboard?.instantiateViewController(withIdentifier: "STPlayerViewController") as! STPlayerViewController
        playerView.url = url
        self.navigationController?.pushViewController(playerView, animated: false)
    }
    
    func isConnectedToInternet() -> Bool {
        let isReachable = NetworkReachabilityManager()!.isReachable
        guard isReachable else {
            STUtils.displayAlert(message: STConstant.STUserDefaultsKeys.NO_INTERNET, themeStyle: .warning)
            return false
        }
        return isReachable
    }
    
    func startActivityAnimating(message: String) {
        if self.isAnimating{
            stopActicityAnimating()
        }
        let size = CGSize(width: 40, height: 40)
        startAnimating(size, message: message, type: NVActivityIndicatorType.orbit)
    }
    
    func stopActicityAnimating() {
        self.stopAnimating()
    }
}
