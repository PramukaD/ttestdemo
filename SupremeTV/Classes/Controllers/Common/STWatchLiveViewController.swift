//
//  STWatchLiveViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/30/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import WebKit

class STWatchLiveViewController: STBaseViewController {

    @IBOutlet weak var containerView: UIView!
       
    private let webView = WKWebView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePlayerView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIViewController.attemptRotationToDeviceOrientation()
    }
    
    private func configurePlayerView(){
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.myOrientation = .landscape
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        loadLiveUrl()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidLeaveFullscreen(_:)), name: UIWindow.didBecomeHiddenNotification, object: view.window)
    }
    
    @objc private func onDidLeaveFullscreen(_ notification: Notification) {
        self.dismissView()
    }
    
    private func dismissView(){
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        NotificationCenter.default.post(name: Notification.Name(STConstant.STUserDefaultsKeys.RELOAD_HOME_NOTIFICATION), object: nil)
        showHomeView()
    }
    
    private func showHomeView(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showHomeView()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismissView()
    }
    
    private func loadLiveUrl(){
        webView.backgroundColor = .black
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.containerView.addSubview(self.webView)
        NSLayoutConstraint.activate([
                    self.webView.leftAnchor.constraint(equalTo: self.containerView.leftAnchor),
                    self.webView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor),
                    self.webView.rightAnchor.constraint(equalTo: self.containerView.rightAnchor),
                    self.webView.topAnchor.constraint(equalTo: self.containerView.topAnchor),
                    ])
        self.containerView.setNeedsLayout()
        let link = URL(string:"https://player.twitch.tv/?channel=supremetvlk")!
        let request = URLRequest(url: link)
        webView.load(request)
    }
}
