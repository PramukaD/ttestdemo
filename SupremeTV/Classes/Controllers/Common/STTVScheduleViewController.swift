//
//  STTVScheduleViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/5/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STTVScheduleViewController: STBaseViewController {

    @IBOutlet weak var tvScheduleTableView: UITableView!
    @IBOutlet weak var dateButton: UIButton!

    var scheduleArray = [ScheduleModel]()
    var didSelectDate = Date()
    var didSelectDateString: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTVScheduleView()
        getScheduleProgrammes()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        dateButton.setButtonGradient(width: 125.0)
        dateButton.clipsToBounds = true
    }
}

// MARK: Search API

extension STTVScheduleViewController{
    
    private func getScheduleProgrammes(){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getScheduleProgrammes(withDate: self.didSelectDateString, onSuccess: {(_ programmes: [ScheduleModel]) -> Void in
            self.stopActicityAnimating()
            if self.scheduleArray.count != 0 { self.scheduleArray.removeAll() }
            self.scheduleArray = programmes
            self.tvScheduleTableView.reloadData()
            self.tvScheduleTableView.isHidden = (self.scheduleArray.count != 0) ? false : true
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }
}

// MARK: Configure TVSchedule View

extension STTVScheduleViewController{
    
    private func configureTVScheduleView(){
        self.setDateFormatter()
        self.tvScheduleTableView.isHidden = true
        self.tvScheduleTableView.register(UINib(nibName: "STTVScheduleTableViewCell", bundle: nil), forCellReuseIdentifier: "STTVScheduleTableViewCell")
        self.tvScheduleTableView.dataSource = self
        self.tvScheduleTableView.delegate = self
    }
    
    @IBAction func revealMenuButtonAction(_ sender: Any) {
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }
    
    func setDateFormatter(){
        self.didSelectDateString = STUtils.getFormattedDate(date: self.didSelectDate)
        self.dateButton.setTitle(self.didSelectDateString, for: .normal)
        getScheduleProgrammes()
    }
    
    @IBAction func dateButtonAction(_ sender: Any) {
        DPPickerManager.shared.showPicker(title: "CHOOSE_DATE".localized, selected: self.didSelectDate, completion: {(date, cancel) in
            if !cancel {
                if self.didSelectDate != date!{
                    self.didSelectDate = date!
                    self.setDateFormatter()
                }
            }
        })
    }
    
}

// MARK: UITableViewDelegate, UITableViewDataSource

extension STTVScheduleViewController: UITableViewDelegate, UITableViewDataSource{
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.scheduleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "STTVScheduleTableViewCell") as! STTVScheduleTableViewCell
        cell.setupCell(aSchedule: self.scheduleArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let aSchedule = self.scheduleArray[indexPath.row]
        self.navigateToProgrammeDeatilView(aProgramme: aSchedule.tvShow!)
    }
}
