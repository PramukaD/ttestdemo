//
//  STVideoThumbnailPreviewViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/6/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import AVFoundation

protocol STVideoThumbnailPreviewDelegate: class {
    func setVideoTitle(title: String, videoUrl: URL, thumbnail: UIImage)
}

class STVideoThumbnailPreviewViewController: UIViewController {

    weak var delegate: STVideoThumbnailPreviewDelegate?

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var titleTextFeld: UITextField!

    var videoUrl: NSURL?
    var thumImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureVideoThumbnailPreviewView()
    }

}

extension STVideoThumbnailPreviewViewController{
    
    private func configureVideoThumbnailPreviewView(){
        titleTextFeld.setLeftPaddingPoints(15.0)
        titleTextFeld.placeholder = "ENTER_VIDEO_TITLE".localized
        addButton.setButtonGradient(width:view.bounds.width - 40)
        addButton.clipsToBounds = true
        self.videoSnapshot()
    }
    
    private func videoSnapshot() {
        do
        {
            let asset = AVURLAsset(url: self.videoUrl! as URL)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at:CMTimeMake(value: Int64(0), timescale: Int32(1)),actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            thumImage = thumbnail
            self.thumbnail.image = thumbnail
        }
        catch let error as NSError
        {
            print("Error generating thumbnail: \(error)")
        }
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        let (validationStatus, message) = self.validateTitle()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        self.delegate?.setVideoTitle(title: self.titleTextFeld.text!, videoUrl: self.videoUrl! as URL, thumbnail: thumImage)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

// MARK: Validate Email

extension STVideoThumbnailPreviewViewController{
    
    private func validateTitle() -> (Bool, String) {
        guard !self.titleTextFeld.isEmpty() else {
            return (false, "PLEASE_ENTER_VIDEO_TITLE".localized)
        }
        return (true, "")
    }
    
}
