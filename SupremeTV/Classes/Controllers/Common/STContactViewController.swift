//
//  STContactViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/12/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STContactViewController: STBaseViewController {

    @IBOutlet var labels: [UILabel]!
    
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var versionTopSpace: NSLayoutConstraint!
    @IBOutlet weak var emailTopSpace: NSLayoutConstraint!
    @IBOutlet var labelsSpace: [NSLayoutConstraint]!
    @IBOutlet var companyNameLabeltopBottomSpace: [NSLayoutConstraint]!

    override func viewDidLoad() {
        super.viewDidLoad()
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        self.appVersionLabel.text = "APP_VERSION".localized + " \(appVersion ?? "")"
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{ return }
        self.configureContactView()
    }
    
    @IBAction func revealMenuButtonAction(_ sender: Any) {
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }

    @IBAction func rateButtonAction(_ sender: Any) {
       if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "1491323175") {
           if #available(iOS 10, *) {
               UIApplication.shared.open(url, options: [:], completionHandler: nil)

           } else {
               UIApplication.shared.openURL(url)
           }
       }
    }
}

extension STContactViewController{
    
    private func configureContactView(){
        for aLabel in self.labels{
            aLabel.font = UIFont.systemFont(ofSize: 15.0)
        }
        self.rateButton.titleLabel?.font = UIFont.systemFont(ofSize: 15.0)
        self.companyNameLabel.font = UIFont.boldSystemFont(ofSize: 19)

        self.imageHeight.constant = 80.0
        self.imageWidth.constant = 250.0
        self.imageBottomSpace.constant = 35.0
        self.versionTopSpace.constant = 75.0
        self.emailTopSpace.constant = 30.0
        for aConstraint in self.labelsSpace{
            aConstraint.constant = 6.0
        }
        for aConstraint in self.companyNameLabeltopBottomSpace{
            aConstraint.constant = 15.0
        }
    }
    
}


