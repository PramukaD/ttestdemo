//
//  STQrScanViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/16/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

protocol STQrScanDelegate: class {
    func setQrData(draw: String)
    func didCancelScanning()
}

class STQrScanViewController: STBaseViewController, AVCaptureMetadataOutputObjectsDelegate {

    weak var delegate: STQrScanDelegate?

    @IBOutlet weak var aContainerView: UIView!
    private var captureSession: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    private var isReading: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleAppEnterForeground), name: Notification.Name("LRConstant.LRMessages.APP_WILL_ENTER_FOREGROUND"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupQrScanning()
        
    }
    
    func setupQrScanning () {
        self.captureSession = nil;
        self.startReading()
    }
    
    @objc func handleAppEnterForeground () {
        if !self.isReading {
            self.setupQrScanning()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func scanningNotPossible(message: String, navigateToSettings: Bool) {
        let alert = UIAlertController(title: "SupremeTV" , message: message , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if navigateToSettings {
                self.navigateToSettings()
            }else {
                self.dismiss(animated: true, completion: nil)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func navigateToSettings()  {
        if let settingsURL = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
        }
    }
    
    func startReading() {
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
            let cameraMediaType = AVMediaType.video
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                DispatchQueue.main.sync {
                    if !granted {
                        self.stopReading()
                        self.setupQrScanning()
                    }
                }
            }
            
        } catch let error as NSError {
            // Handle any errors
            if error.localizedFailureReason == nil {
                self.scanningNotPossible(message: "CAN_NOT_SCAN_QR_CODE".localized, navigateToSettings: false)
            }else {
                if error.localizedFailureReason == "This app is not authorized to use Back Camera." {
                    self.scanningNotPossible(message: "PLEASE_ENABLE_CAMERA_ACCESS".localized, navigateToSettings: true)
                }else {
                    self.scanningNotPossible(message: error.localizedFailureReason!, navigateToSettings: false)
                }
            }
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer.frame = aContainerView.layer.bounds
        aContainerView.layer.addSublayer(videoPreviewLayer)
        
        /* Check for metadata */
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.metadataObjectTypes = captureMetadataOutput.availableMetadataObjectTypes
        print(captureMetadataOutput.availableMetadataObjectTypes)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureSession?.startRunning()
        
        return
    }
    
    func stopReading() {
        self.isReading = false;
        self.captureSession?.stopRunning()
        self.captureSession = nil
        
        if self.videoPreviewLayer != nil {
            self.videoPreviewLayer.removeFromSuperlayer()
        }
    }
    
    public func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        for data in metadataObjects {
            let metaData = data
            print(metaData.description)
            let transformed = videoPreviewLayer?.transformedMetadataObject(for: metaData) as? AVMetadataMachineReadableCodeObject
            if let unwraped = transformed {
                if self.validateQrCodeData(text: unwraped.stringValue!) {
                    self.stopReading()
                    self.delegate?.setQrData(draw: unwraped.stringValue!)
                    self.dismiss(animated: true, completion: nil)
                }else {
                }
            }
        }
    }
    
    private func validateQrCodeData(text: String) -> Bool {
        if text.isEmpty{
            return false
        }
        return true
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.delegate?.didCancelScanning()
        self.dismiss(animated: true, completion: nil)
    }
}
