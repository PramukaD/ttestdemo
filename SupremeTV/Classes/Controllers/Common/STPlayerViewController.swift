//
//  STPlayerViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/10/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class STPlayerViewController: UIViewController {

    var url:String?
    
    @IBOutlet var videoPlayer: WKYTPlayerView!
    @IBOutlet var backButton: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configurePlayerView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIViewController.attemptRotationToDeviceOrientation()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismissView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIWindow.didBecomeVisibleNotification, object: view.window)
        NotificationCenter.default.removeObserver(self, name: UIWindow.didBecomeHiddenNotification, object: view.window)
    }
}

// MARK: Configure YouTubePlayer

extension STPlayerViewController{
    
    private func configurePlayerView(){
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.myOrientation = .landscape
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        
        videoPlayer.delegate = self
        videoPlayer.load(withVideoId: getYoutubeIdd())
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidEnterFullscreen(_:)), name: UIWindow.didBecomeVisibleNotification, object: view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidLeaveFullscreen(_:)), name: UIWindow.didBecomeHiddenNotification, object: view.window)
    }
    
    func getYoutubeIdd() -> String {
        let id = URLComponents(string: self.url!)?.queryItems?.first(where: { $0.name == "v" })?.value ?? ""
        return id
    }
    
    @objc func onDidEnterFullscreen(_ notification: Notification) {}

    @objc func onDidLeaveFullscreen(_ notification: Notification) {
        self.dismissView()
    }
    
    private func dismissView(){
        let appDel = UIApplication.shared.delegate as! AppDelegate
        appDel.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        NotificationCenter.default.post(name: Notification.Name(STConstant.STUserDefaultsKeys.RELOAD_HOME_NOTIFICATION), object: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
}

// MARK: WKYTPlayerViewDelegate

extension STPlayerViewController: WKYTPlayerViewDelegate {
    
    func playerViewDidBecomeReady(_ playerView: WKYTPlayerView) {
        self.stopActicityAnimating()
        playerView.playVideo()
    }
    
    func playerViewPreferredWebViewBackgroundColor(_ playerView: WKYTPlayerView) -> UIColor {
         return .black
     }
     
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        print(state)
        switch state {
        case .unknown:
            print("UNKNOWNERROR")
            self.backButton.isHidden = false
            break
        default:
            break
        }
    }
    
    func playerViewPreferredInitialLoading(_ playerView: WKYTPlayerView) -> UIView? {
         self.startActivityAnimating(message: "")
         let aView = UIView.init(frame: self.view.bounds)
         aView.backgroundColor = .black
         return aView
     }
     
     func playerView(_ playerView: WKYTPlayerView, receivedError error: WKYTPlayerError) {
         STUtils.displayAlert(message: STConstant.STUserDefaultsKeys.COMMON_ERROR_MESSAGE, themeStyle: .error)
         self.dismissView()
     }
    
    func playerView(_ playerView: WKYTPlayerView, didPlayTime playTime: Float) {
        if playTime >= 1{
            backButton.isHidden = true
        }
    }
}
