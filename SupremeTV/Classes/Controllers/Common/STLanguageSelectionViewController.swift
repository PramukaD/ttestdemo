//
//  STLanguageSelectionViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 1/6/20.
//  Copyright © 2020 Persystance Networks. All rights reserved.
//

import UIKit

class STLanguageSelectionViewController: UIViewController {

    @IBOutlet var selectImageViews: [UIImageView]!
    @IBOutlet var selectButtonss: [UIButton]!
    
    private var didSelectLguageIndex = 0
    private var previousDidSelectLguageIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    @IBAction func languageSelectButtonAction(_ sender: Any) {
        let tappedButton = sender as! UIButton
        didSelectLguageIndex = tappedButton.tag
        self.updateUI()
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        updateLanguage()
        dismissView()
    }
    
    private func configureView(){
        let lan = STLanguageManager.shared.currentLanguage
        switch lan.id {
        case 40: didSelectLguageIndex = 40; break
        case 41: didSelectLguageIndex = 41; break
        default: didSelectLguageIndex = 42; break
        }
        self.previousDidSelectLguageIndex = didSelectLguageIndex
        self.updateUI()
    }
    
    private func updateUI(){
        for anImageView in self.selectImageViews {
            if anImageView.tag == self.didSelectLguageIndex {
                anImageView.image = UIImage.init(named: "language_check_icon")
            }else{
                anImageView.image = UIImage.init(named: "")
            }
        }
    }
    
    private func updateLanguage(){
        let manager = STLanguageManager.shared
        switch self.didSelectLguageIndex {
        case 40: manager.changeLanguage(lang: manager.languagesData[0]); break
        case 41: manager.changeLanguage(lang: manager.languagesData[1]); break
        default: // manager.changeLanguage(lang: manager.languagesData[2]);
            break
        }
    }
    
    private func dismissView(){
        if self.didSelectLguageIndex != self.previousDidSelectLguageIndex{
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showHomeView()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
