//
//  STProgrammeDetailViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import DPArrowMenuKit

class STProgrammeDetailViewController: STBaseViewController {

    @IBOutlet weak var programmeDetailTableView: UITableView!

    let programmeDetailViewModel = STProgrammeDetailViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureProgrammeDetailView()
        getProgrammeDetailViewInfo()
    }
    
    private func getProgrammeDetailViewInfo(){
        if !isConnectedToInternet(){ return }
        getTvShow()
        getTrendingshows()
    }
    
}

// MARK: API

extension STProgrammeDetailViewController{
    
    private func getTvShow(){
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getTvShow(withId: self.programmeDetailViewModel.didSelectProgramme._id ?? "" , onSuccess: {(_ data: TvShowModel) -> Void in
            self.stopActicityAnimating()
            self.programmeDetailViewModel.didSelectProgramme = data
            self.programmeDetailViewModel.configureSeasonsEpisodesDataSource()
            self.getComments()
            self.programmeDetailTableView.reloadData()
            self.programmeDetailTableView.isHidden = false
        }, onError: {(_ message) -> Void in
            STUtils.displayAlert(message: message, themeStyle: .error)
            self.stopActicityAnimating()
        })
    }
    
    private func createComment(){
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.createComment(commentData: self.programmeDetailViewModel.getNewComment() , onSuccess: {(_ data: ShowCommentModel) -> Void in
            self.stopActicityAnimating()
            self.programmeDetailViewModel.commentsArray.append(data)
            self.programmeDetailViewModel.comment = ""
            self.programmeDetailTableView.reloadData()
        }, onError: {(_ message) -> Void in
            STUtils.displayAlert(message: message, themeStyle: .error)
            self.stopActicityAnimating()
        })
    }
        
    private func getComments(){
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getComments(withId: self.programmeDetailViewModel.didSelectProgramme._id! , onSuccess: {(_ comments: [ShowCommentModel]) -> Void in
            self.stopActicityAnimating()
            if self.programmeDetailViewModel.commentsArray.count != 0 { self.programmeDetailViewModel.commentsArray.removeAll() }
            self.programmeDetailViewModel.commentsArray = comments
            self.programmeDetailTableView.reloadData()
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }
    
    private func getTrendingshows(){
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getTrendingshows(onSuccess: {(_ trendingSowsArray: [TvShowModel]) -> Void in
            self.stopActicityAnimating()
            if self.programmeDetailViewModel.trendingArray.count != 0 { self.programmeDetailViewModel.trendingArray.removeAll() }
            self.programmeDetailViewModel.trendingArray = trendingSowsArray
            self.programmeDetailTableView.reloadData()
            self.scrollToTop()
        }, onError: {(_ message) -> Void in
            self.scrollToTop()
            self.stopActicityAnimating()
        })
    }
}

// MARK: Configure programme detail View

extension STProgrammeDetailViewController{
    
    private func configureProgrammeDetailView(){
        self.programmeDetailTableView.isHidden = true
        self.programmeDetailTableView.dataSource = self
        self.programmeDetailTableView.delegate = self
    }
    
    private func reloadSections(indexset: IndexSet){
        self.programmeDetailTableView.beginUpdates()
        self.programmeDetailTableView.reloadSections(indexset, with: .none)
        self.programmeDetailTableView.endUpdates()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func scrollToTop(){
        let indexPath = NSIndexPath(row: 0, section: 0)
        self.programmeDetailTableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
    }
}

// MARK: Banner cell button actions

extension STProgrammeDetailViewController{
    
    @objc private func watchButtonAction() {
        self.navigateToPlayerView(url: STUtils.getPlayerUrl(tvShow: self.programmeDetailViewModel.didSelectProgramme))
    }
    
    @objc private func seasonsButtonAction(_ sender: Any) {
        if self.programmeDetailViewModel.seasonsMenuArray.count >= 2{
            guard let view = sender as? UIView else { return }
            DPArrowMenu.show(view, viewModels: self.programmeDetailViewModel.seasonsMenuArray, done: { index in
                    self.programmeDetailViewModel.configureDidSelectSeason(index: index)
                    self.programmeDetailTableView.reloadData()
            }) {}
        }
    }
}

// MARK: Create a comment

extension STProgrammeDetailViewController{
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        self.programmeDetailViewModel.comment = (textField.text?.trim())!
    }
    
    @objc private func loginButtonAction() {
        let loginView = self.storyboard?.instantiateViewController(withIdentifier: "STLoginViewController") as! STLoginViewController
        loginView.isFromSignUp = true
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
    @objc private func postButtonAction() {
        let (validationStatus, message) = self.programmeDetailViewModel.validateComment()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        self.createComment()
    }
    
}

// MARK: Home TableView UITableViewDataSource, UITableViewDelegate

extension STProgrammeDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("STHomeTableHeaderView", owner: nil, options: nil)?[0] as? STHomeTableHeaderView
        headerView?.setLabelWidth(headerText: "TRENDING_SHOWS".localized)
        return headerView
    }
       
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (section != 4) ? 0 : (self.programmeDetailViewModel.trendingArray.count != 0) ? 65 : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return (self.programmeDetailViewModel.episodesArray.count == 0) ? 0 : self.programmeDetailViewModel.episodesArray.count
        case 2:
            return 1
        case 3:
            return (self.programmeDetailViewModel.commentsArray.count == 0) ? 0 : self.programmeDetailViewModel.commentsArray.count
        case 4:
            return (self.programmeDetailViewModel.trendingArray.count == 0) ? 0 : 1
        default:
            break
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return configurProgrammeDetailBannerCell(tableView: tableView, indexPath: indexPath)
        case 1:
            return configurEpisodesCell(tableView: tableView, indexPath: indexPath)
        case 2:
            if !STUtils.isUserLoggedIn(){
                return configurLoginCommentCell(tableView: tableView, indexPath: indexPath)
            }
            return configurCommentCell(tableView: tableView, indexPath: indexPath)
        case 3:
            return configurUserCommentCell(tableView: tableView, indexPath: indexPath)
        default:
            return configuTrendingCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 4 { return (UIDevice.current.screenType == .iPhones_5_5s_5c_SE_4_4S) ? 145.0 : 165.0 }
        return UITableView.automaticDimension
    }
   
    private func configurProgrammeDetailBannerCell(tableView: UITableView, indexPath: IndexPath) -> STProgrammeDetailBannerTableViewCell{
        self.programmeDetailTableView.register(UINib(nibName: "STProgrammeDetailBannerTableViewCell", bundle: nil), forCellReuseIdentifier: "STProgrammeDetailBannerTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STProgrammeDetailBannerTableViewCell") as! STProgrammeDetailBannerTableViewCell
        cell.setupCellInfo(aProgramme: self.programmeDetailViewModel.didSelectProgramme)
        cell.watchButton.addTarget(self, action: #selector(self.watchButtonAction), for: .touchUpInside)
        cell.seasonButton.addTarget(self, action: #selector(self.seasonsButtonAction(_:)), for: .touchUpInside)
        cell.shareButton.addTarget(self, action: #selector(self.shareButtonAction), for: .touchUpInside)
        cell.displaySeasonButton(title: self.programmeDetailViewModel.didSelectSeasonTitle, menuArray: self.programmeDetailViewModel.seasonsMenuArray)
        return cell
    }
    
    private func configurEpisodesCell(tableView: UITableView, indexPath: IndexPath) -> STEpisodeTableViewCell{
        self.programmeDetailTableView.register(UINib(nibName: "STEpisodeTableViewCell", bundle: nil), forCellReuseIdentifier: "STEpisodeTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STEpisodeTableViewCell") as! STEpisodeTableViewCell
        let anEpisode = self.programmeDetailViewModel.episodesArray[indexPath.row]
        cell.setupEpisodeDetails(anEpisode: anEpisode)
        return cell
    }
    
    private func configurLoginCommentCell(tableView: UITableView, indexPath: IndexPath) -> STLoginCommentTableViewCell{
        self.programmeDetailTableView.register(UINib(nibName: "STLoginCommentTableViewCell", bundle: nil), forCellReuseIdentifier: "STLoginCommentTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STLoginCommentTableViewCell") as! STLoginCommentTableViewCell
        cell.loginButton.addTarget(self, action: #selector(self.loginButtonAction), for: .touchUpInside)
        cell.displayCommentLabel(isShown: (self.programmeDetailViewModel.commentsArray.count != 0) ? false : true)
        return cell
    }
    
    private func configurCommentCell(tableView: UITableView, indexPath: IndexPath) -> STCommentTextFieldTableViewCell{
        self.programmeDetailTableView.register(UINib(nibName: "STCommentTextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "STCommentTextFieldTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STCommentTextFieldTableViewCell") as! STCommentTextFieldTableViewCell
        cell.commentTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        cell.postButton.addTarget(self, action: #selector(self.postButtonAction), for: .touchUpInside)
        cell.displayCommentLabel(isShown: (self.programmeDetailViewModel.commentsArray.count != 0) ? false : true)
        cell.commentTextField.text = self.programmeDetailViewModel.comment
        return cell
    }
    
    private func configurUserCommentCell(tableView: UITableView, indexPath: IndexPath) -> STCommentTableViewCell{
        self.programmeDetailTableView.register(UINib(nibName: "STCommentTableViewCell", bundle: nil), forCellReuseIdentifier: "STCommentTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STCommentTableViewCell") as! STCommentTableViewCell
        let aComment = self.programmeDetailViewModel.commentsArray[indexPath.row]
        cell.setupCommentCell(aComment: aComment)
        return cell
    }
    
    private func configuTrendingCell(tableView: UITableView, indexPath: IndexPath) -> STHomeProgrammeTableViewCell{
        self.programmeDetailTableView.register(UINib(nibName: "STHomeProgrammeTableViewCell", bundle: nil), forCellReuseIdentifier: "STHomeProgrammeTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STHomeProgrammeTableViewCell") as! STHomeProgrammeTableViewCell
        cell.reloadProgrammeCollectionView(programmes: self.programmeDetailViewModel.trendingArray, section: 0, bannerSection: 1)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.section == 1{
            let epi = self.programmeDetailViewModel.episodesArray[indexPath.row]
            self.navigateToPlayerView(url: epi.url ?? "")
        }
    }
}

// MARK: Did select Programme / Header

extension STProgrammeDetailViewController: STHomeProgrammeTableViewCellDelegate{
    
    func didSelectProgrammeIndexPath(indexPath: IndexPath, section: Int){
        self.programmeDetailViewModel.self.didSelectSeasonIndex = 0
        let aProgramme = self.programmeDetailViewModel.trendingArray[indexPath.row]
        self.programmeDetailViewModel.didSelectProgramme = aProgramme
        self.getProgrammeDetailViewInfo()
    }
}

extension STProgrammeDetailViewController{
    
    @objc private func shareButtonAction() {
        self.startAnimating()
        var items: [Any] = [self.programmeDetailViewModel.didSelectProgramme.title as Any]
        let postUrl = STUtils.getPlayerUrl(tvShow: self.programmeDetailViewModel.didSelectProgramme)
        if !postUrl.isEmpty {
            items = [self.programmeDetailViewModel.didSelectProgramme.title as Any, URL(string: STUtils.getPlayerUrl(tvShow: self.programmeDetailViewModel.didSelectProgramme)) as Any]
        }
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        let cell = self.programmeDetailTableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as! STProgrammeDetailBannerTableViewCell?
        activityViewController.popoverPresentationController?.sourceView = (cell?.shareButton)
        activityViewController.popoverPresentationController?.permittedArrowDirections =
        UIPopoverArrowDirection(rawValue: 0)
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width:
        0, height: 0)
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.message,
            UIActivity.ActivityType.mail,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.copyToPasteboard,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo,
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.openInIBooks,
            UIActivity.ActivityType(rawValue: "com.apple.reminders.RemindersEditorExtension"),
            UIActivity.ActivityType(rawValue: "com.apple.mobilenotes.SharingExtension")
        ]
        self.present(activityViewController, animated: true, completion: { () -> Void in
            self.stopAnimating()
        })
    }
}
