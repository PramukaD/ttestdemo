//
//  STSideMenuViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/4/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

struct STSideMenu {
    var icon: String?
    var menuTitle: String?
}

class STSideMenuViewController: STBaseViewController {

    @IBOutlet weak var sideMenuTableView: UITableView!
    
    private let menuItemsArray = [
        STSideMenu.init(icon: "ic_watch_live", menuTitle: "SIGNIN".localized),
        STSideMenu.init(icon: "ic_home", menuTitle: "HOME".localized),
        STSideMenu.init(icon: "ic_watch_live", menuTitle: "WATCH_LIVE".localized),
        STSideMenu.init(icon: "ic_schedule", menuTitle: "TV_SCHEDULE".localized),
        STSideMenu.init(icon: "ic_settings", menuTitle: "SETTINGS".localized),
        STSideMenu.init(icon: "ic_my_draws", menuTitle: "MY_DRAWS".localized),
        STSideMenu.init(icon: "ic_contact_us", menuTitle: "CONTACT_US".localized),
        STSideMenu.init(icon: "ic_uplaod_video", menuTitle: "UPLOAD_VIDEOS".localized),
        STSideMenu.init(icon: "ic_sign_out", menuTitle: "SIGN_OUT_SIMPLE".localized),
        STSideMenu.init(icon: "", menuTitle: "")
    ]
    
    var menuSelectedIndexPath: ((IndexPath)-> Void)!
    var profileLoginRegistration: ((Bool)-> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSideMenuTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.delegate = self
    }
    
}

//MARK: Setup Table view

extension STSideMenuViewController{
    
    private func setupSideMenuTableView(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadMenuTable), name: Notification.Name(STConstant.STUserDefaultsKeys.RELOAD_SIDE_MENU_NOTIFICATION), object: nil)
        self.sideMenuTableView.delegate = self;
        self.sideMenuTableView.dataSource = self;
        self.sideMenuTableView.reloadData()
    }
    
    @objc private func reloadMenuTable(){
        self.sideMenuTableView.reloadData()
    }
    
}

//MARK: Side menu button actions

extension STSideMenuViewController{

    private func scrollToTop() {
        let indexPath = NSIndexPath(row: 0, section: 0)
        self.sideMenuTableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
    }
    
    @objc private func signupButtonAction() {
        self.profileLoginRegistration(false)
    }
       
    @objc private func loginButtonAction() {
        self.profileLoginRegistration(true)
    }
    
    @objc private func profileButtonAction() {
        UserDefaults.standard.setValue(0, forKey: STConstant.STUserDefaultsKeys.SIDE_MENU_SELECTED_INDEX)
        self.menuSelectedIndexPath(IndexPath.init(row: 4, section: 0))
    }
    
    @objc private func socialButtonsAction(sender: UIButton!) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        switch sender.tag {
        case 21:
            appDelegate.navigateToSocialPages(socialPageUrl: "https://www.facebook.com/supremeTVLK/")
            break
        case 22:
            appDelegate.navigateToSocialPages(socialPageUrl: "https://www.youtube.com/channel/UCSsTFW6gPcjm63Fw86gZdSw")
            break
        case 23:
            appDelegate.navigateToSocialPages(socialPageUrl: "https://instagram.com/supremetv.lk")
            break
        case 24:
            appDelegate.navigateToSocialPages(socialPageUrl: "https://twitter.com/SupremeTVLK")
            break
//        case 24:
//            appDelegate.navigateToSocialPages(socialPageUrl: "https://www.pinterest.com/supremetvlk")
//            break
//        case 25:
//            appDelegate.navigateToSocialPages(socialPageUrl: "https://www.tumblr.com/blog/supremetvlk")
//            break
        default:
            break
        }
    }
    
    private func logout(){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userLogout(onSuccess: {(_ data: CommonSuccess) -> Void in
            self.stopActicityAnimating()
            self.scrollToTop()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.userLogout()
        }, onError: {(_ message) -> Void in self.stopActicityAnimating() })
    }
    
}


//MARK: UITableViewDataSource, UITableViewDelegate

extension STSideMenuViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return menuItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.row == 0{
            self.sideMenuTableView.register(UINib(nibName: "STProfileSideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "STProfileSideMenuTableViewCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "STProfileSideMenuTableViewCell") as! STProfileSideMenuTableViewCell
            cell.configureCell()
            self.setProfieCellButtonsAction(cell: cell)
            cell.profileButton.addTarget(self, action: #selector(self.profileButtonAction), for: .touchUpInside)
            return cell
        }
        self.sideMenuTableView.register(UINib(nibName: "STSideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "STSideMenuTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STSideMenuTableViewCell") as! STSideMenuTableViewCell
        if indexPath.row == menuItemsArray.count - 1 {
            cell.setupSocialMediaCell()
            self.setSocialButtonsAction(cell: cell)
        }else{
            cell.setupCell(indexPath: indexPath, menu: menuItemsArray[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 {
            return (!STUtils.isUserLoggedIn()) ? 265.0 : 215.0
        }
        if indexPath.row == 5 {
            return 0
        }
        return (indexPath.row == menuItemsArray.count - 1) ? 155.0 : 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        if indexPath.row == 2 {
//            self.openYoutubeApp()
//            return
//        }
        if !STUtils.isUserLoggedIn(){
            if indexPath.row == 6 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 {
                UserDefaults.standard.setValue(indexPath.row, forKey: STConstant.STUserDefaultsKeys.SIDE_MENU_SELECTED_INDEX)
                self.menuSelectedIndexPath(indexPath)
            }else{
                self.profileLoginRegistration(true)
            }
        }else{
            STUtils.clearDirectory()
            if indexPath.row == 8 { self.logout(); return }
            if indexPath.row == 9 { return }
            UserDefaults.standard.setValue(indexPath.row, forKey: STConstant.STUserDefaultsKeys.SIDE_MENU_SELECTED_INDEX)
            self.menuSelectedIndexPath(indexPath)
        }
    }
    
    private func setProfieCellButtonsAction(cell: STProfileSideMenuTableViewCell){
        cell.signupButton.addTarget(self, action: #selector(self.signupButtonAction), for: .touchUpInside)
        cell.loginButton.addTarget(self, action: #selector(self.loginButtonAction), for: .touchUpInside)
    }
    
    private func setSocialButtonsAction(cell: STSideMenuTableViewCell){
        for aButton in cell.socialButtons {
            aButton.addTarget(self, action: #selector(self.socialButtonsAction(sender:)), for: .touchUpInside)
        }
    }
    
    private func openYoutubeApp() {
        let appURL = NSURL(string: STConstant.STUrl.APP_LIVE_URL)
        let application = UIApplication.shared
        if application.canOpenURL(appURL! as URL) {
            application.open(appURL! as URL)
        } else {
            application.open(NSURL(string: STConstant.STUrl.WEB_LIVE_URL)! as URL)
        }
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.toggleLeftSlidingPanel), userInfo: nil, repeats: false)
    }
    
    @objc private func toggleLeftSlidingPanel(){
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }
}
