//
//  STUploadVideoViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/6/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

struct STVideoUpload {
    var title: String
    var videoUrl: URL
    var thumbnail: UIImage
}

class STUploadVideoViewController: STBaseViewController {

    @IBOutlet weak var uploadVideoCollectionView: UICollectionView!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var uploadingView: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!

    private var progressValue = 0.0

    private var videoTitle: String = ""
    private var videoDescription: String = ""
    private var imagePicker = UIImagePickerController()

    private var videoArray = [STVideoUpload]()
    private var videoTitleModel = VideoTitleModel()
    
    private var uploadingIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUploadVideoView()
    }
    
}


// MARK: Configure TVSchedule View

extension STUploadVideoViewController{
    
    private func configureUploadVideoView(){
        self.uploadingView.isHidden = true
        uploadButton.setButtonGradient(width:view.bounds.width - 40)
        uploadButton.clipsToBounds = true
        configureCollectionView()
    }
    
    private func configureCollectionView(){
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 10 // =
        layout.minimumInteritemSpacing = 7  // ||
        self.uploadVideoCollectionView.setCollectionViewLayout(layout, animated: true)
        
        self.uploadVideoCollectionView.dataSource = self
        self.uploadVideoCollectionView.delegate = self
    }
    
    @IBAction func revealMenuButtonAction(_ sender: Any) {
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }
    
    @IBAction func uploadButtonAction(_ sender: Any) {
        let (validationStatus, message) = self.validateVideo()
        guard validationStatus else {
            STUtils.displayAlert(message: message, themeStyle: .error)
            return
        }
        createVideoTitle()
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text?.trim()
        if textField.tag == 20 { self.videoTitle = text! }else { self.videoDescription = text! }
    }
    
    @objc private func galleyButtonAction() {
        self.configureVideoLibrary()
    }
    
    @objc private func galleyVideoCloseButtonAction(_ sender: UIButton) {
        let index = sender.tag
        self.videoArray.remove(at: index)
        self.uploadVideoCollectionView.reloadData()
    }
    
    private func videoUploadingProcess(){
        let aVideo = self.videoArray[self.uploadingIndex]
        self.uploadingIndex += 1
        self.view.bringSubviewToFront(self.uploadingView)
        self.uploadingView.isHidden = false
        self.uploadVideo(video: aVideo.videoUrl, titleId: self.videoTitleModel._id!, name: aVideo.title)
    }
    
    private func resetVideoUploadingView(){
        self.uploadingIndex = 0
        self.videoArray.removeAll()
        self.videoTitle = ""
        self.videoDescription = ""
        self.view.bringSubviewToFront(self.uploadVideoCollectionView)
        self.uploadVideoCollectionView.reloadData()
        self.uploadingView.isHidden = true
    }
    
    private func setProgressValue(){
        self.progressBar.setProgress(Float(Float(self.uploadingIndex) / Float(self.videoArray.count)), animated: true) // self.uploadingIndex /  self.videoArray.count
    }
}

// MARK: API

extension STUploadVideoViewController{
    
    private func createVideoTitle(){
        if !isConnectedToInternet(){ return }
        var aVideo = VideoTitleModel()
        aVideo.title = self.videoTitle
        aVideo._description = self.videoDescription
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.createVideoTitle(withVideoTitle: aVideo, onSuccess: {(_ videoModel: VideoTitleModel) -> Void in
            self.stopActicityAnimating()
            self.videoTitleModel = videoModel
            self.videoUploadingProcess()
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }
    
    private func uploadVideo(video: URL, titleId: String, name: String){
        self.progressLabel.text = "Uploading \(self.uploadingIndex) of \(self.videoArray.count)"
        self.setProgressValue()
        SupremeTVAPI.uploadVideo(video: video, titleId: titleId, name: name, onSuccess: {(_ videoModel: VideoModel) -> Void in
            if self.videoArray.count != self.uploadingIndex{
                self.videoUploadingProcess()
            }else{
                self.resetVideoUploadingView()
                STUtils.displayAlert(message: "VIDEOS_UPLOADED__SUCCESSFULLY".localized, themeStyle: .success)
            }
        }, onError: {(_ message) -> Void in
            self.uploadingIndex = 0
            self.uploadingView.isHidden = true
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
    
}

// MARK: Validate user login credentials

extension STUploadVideoViewController{
    
    private func validateVideo() -> (Bool, String) {
        guard !self.videoTitle.isEmpty else {
            return (false, "PLEASE_ENTER_VIDEO_TITLE".localized)
        }
        guard !self.videoDescription.isEmpty else {
            return (false, "PLEASE_ENTER_VIDEO_DESCRIPTION".localized)
        }
        guard self.videoArray.count != 0 else {
            return (false, "PLEASE_ADD_VIDEOS".localized)
        }
        return (true, "")
    }
    
}

// MARK: Upload Video Collection View UICollectionViewDataSource

extension STUploadVideoViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return self.videoArray.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            self.uploadVideoCollectionView.register(UINib(nibName: "STUploadVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STUploadVideoCollectionViewCell")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STUploadVideoCollectionViewCell", for: indexPath) as! STUploadVideoCollectionViewCell
            cell.galleryButton.addTarget(self, action: #selector(self.galleyButtonAction), for: .touchUpInside)
            cell.titleTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            cell.descriptionTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            cell.titleTextField.text = self.videoTitle
            cell.descriptionTextField.text = self.videoDescription
            return cell
        }
        self.uploadVideoCollectionView.register(UINib(nibName: "STHomeProgrammeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "STHomeProgrammeCollectionViewCell")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STHomeProgrammeCollectionViewCell", for: indexPath) as! STHomeProgrammeCollectionViewCell
        cell.setupVideoDetails(aVideo: self.videoArray[indexPath.row])
        cell.setTitleTextAlignment()
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(self.galleyVideoCloseButtonAction), for: .touchUpInside)
        return cell
    }
    
}

// MARK: Upload Video Collection View UICollectionViewDelegateFlowLayout

extension STUploadVideoViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: collectionView.frame.width, height:250.0)
        }
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        guard UIDevice.current.screenType != .iPhones_5_5s_5c_SE_4_4S  else{
            return CGSize(width:widthPerItem, height:125.0)
        }
        return CGSize(width:widthPerItem, height:145.0)
    }
}

// MARK: Select a video

extension STUploadVideoViewController: UIImagePickerControllerDelegate, STVideoThumbnailPreviewDelegate{
    
   private func configureVideoLibrary(){
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self;
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.mediaTypes = ["public.movie"]
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        picker.dismiss(animated: true, completion: nil)
        guard let videoUrl = info[.mediaURL] as? NSURL else {
            return
        }
        self.dispalyVideoPreview(videoUrl: videoUrl as URL)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func dispalyVideoPreview(videoUrl: URL){
         let previewView = self.storyboard?.instantiateViewController(withIdentifier: "STVideoThumbnailPreviewViewController") as! STVideoThumbnailPreviewViewController
        previewView.videoUrl = videoUrl as NSURL
        previewView.delegate = self
        self.present(previewView, animated: true, completion: nil)
    }
    
    func setVideoTitle(title: String, videoUrl: URL, thumbnail: UIImage){
        let aVideo = STVideoUpload.init(title: title, videoUrl: videoUrl, thumbnail: thumbnail)
        self.videoArray.append(aVideo)
        self.uploadVideoCollectionView.reloadData()
    }
}


