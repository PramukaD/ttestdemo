//
//  STMyDrawsViewController.swift
//  SupremeTV
//
//  Created by Persystance Networks on 12/12/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import CoreLocation

enum STDrawStatus {
    case stdefault
    case applied
    case notapplied
}
class STMyDrawsViewController: STBaseViewController, STQrScanDelegate {

    @IBOutlet weak var myDrawsTableView: UITableView!
    private var dashboardDraw = DrawModel()
    private var drawArray = [UserApplyDrawModel]()
    private var dashboardStatus: STDrawStatus = .stdefault
    private var locationManager:CLLocationManager!
    private var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMyDrawsTableViewAndLocationService()
        getDashboardDraw()
    }
        
    @IBAction func revealMenuButtonAction(_ sender: Any) {
        STMenuOptionManager.sharedInstance.slidingPanel.toggleLeftSlidingPanel()
    }

    @objc func qrButtonButtonAction() {
        self.setupCoreLocation()
        let qrView = self.storyboard?.instantiateViewController(withIdentifier: "STQrScanViewController") as! STQrScanViewController
        qrView.delegate = self
        self.present(qrView, animated: true, completion: nil)
    }
    
    private func setAppliedStatus(){
        for aDraw in self.drawArray{
            if aDraw.draw?._id == self.dashboardDraw._id{
                dashboardStatus = .applied
                return
            }
        }
        if self.dashboardDraw._id != nil{
            dashboardStatus = .notapplied
        }
    }
    
}

// MARK: API

extension STMyDrawsViewController{
    
    private func getUserApplyDraw(){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getUserApplyDraw(onSuccess: {(_ data: [UserApplyDrawModel]) -> Void in
            self.stopActicityAnimating()
            self.drawArray = data
            self.setAppliedStatus()
            self.myDrawsTableView.reloadData()
            self.myDrawsTableView.isHidden = false
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
        })
    }
    
    private func userApplyDraw(drawData: DrawToUserModel){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.userApplyDraw(drawData: drawData, onSuccess: {(_ data: DrawToUserModel) -> Void in
            self.stopActicityAnimating()
            self.dashboardStatus = .stdefault
            self.getDashboardDraw()
            STUtils.displayAlert(message: "SUCCESSFULLY_ACTIVATED_THE_CAPAIGN".localized, themeStyle: .success)
        }, onError: {(_ message) -> Void in
            self.stopActicityAnimating()
            STUtils.displayAlert(message: message, themeStyle: .error)
        })
    }
    
    private func getDashboardDraw(){
        if !isConnectedToInternet(){ return }
        self.startActivityAnimating(message: STConstant.STUserDefaultsKeys.LOADING_TEXT)
        SupremeTVAPI.getDashboardDraw(onSuccess: {(_ data: DrawModel) -> Void in
            self.dashboardDraw = data
            self.stopActicityAnimating()
            self.myDrawsTableView.reloadData()
            self.myDrawsTableView.isHidden = false
            self.getUserApplyDraw()
        }, onError: {(_ message) -> Void in
            self.getUserApplyDraw()
            self.stopActicityAnimating()
        })
    }

}

//MARK: Setup Table view / Location

extension STMyDrawsViewController{
    
    private func setupMyDrawsTableViewAndLocationService(){
        if !CLLocationManager.locationServicesEnabled(){
            setupCoreLocation()
        }
        self.myDrawsTableView.isHidden = true
        self.myDrawsTableView.delegate = self;
        self.myDrawsTableView.dataSource = self;
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
}

//MARK: UITableViewDataSource, UITableViewDelegate

extension STMyDrawsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("STHomeTableHeaderView", owner: nil, options: nil)?[0] as? STHomeTableHeaderView
        headerView?.configureDrawHeader()
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 { return 0 }
        return 65.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 { return 1 }
        return (self.drawArray.count == 0) ? 1 : self.drawArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0{
            self.myDrawsTableView.register(UINib(nibName: "STDrawCampaignTableViewCell", bundle: nil), forCellReuseIdentifier: "STDrawCampaignTableViewCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "STDrawCampaignTableViewCell") as! STDrawCampaignTableViewCell
            cell.qrButton.addTarget(self, action: #selector(self.qrButtonButtonAction), for: .touchUpInside)
            cell.configureDrawCell(aDraw: self.dashboardDraw, dashboardStatus: self.dashboardStatus)
            return cell
        }
        self.myDrawsTableView.register(UINib(nibName: "STMyDrawsTableViewCell", bundle: nil), forCellReuseIdentifier: "STMyDrawsTableViewCell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "STMyDrawsTableViewCell") as! STMyDrawsTableViewCell
        if self.drawArray.count != 0{
            cell.configureDrawCell(aDraw:self.drawArray[indexPath.row])
        }else{
            cell.configureEmptyDraw()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 1 && self.drawArray.count == 0{ return 230.0}
        return UITableView.automaticDimension
    }

}

// MARK: Location

extension STMyDrawsViewController: CLLocationManagerDelegate{
    
    private func setupCoreLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation = locations[0] as CLLocation
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func didCancelScanning(){
        locationManager.stopUpdatingLocation()
    }
        
    func setQrData(draw: String){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    STUtils.displayAlert(message: "PLEASE_ENTER_LOCATION_SERVICE".localized, themeStyle: .error)
                    return
                case .authorizedAlways, .authorizedWhenInUse:
                    var aDraw = DrawToUserModel()
                    aDraw.draw = draw
                    aDraw.latitude = "\(self.currentLocation.coordinate.latitude)"
                    aDraw.longitude = "\(self.currentLocation.coordinate.longitude)"
                    userApplyDraw(drawData: aDraw)
                @unknown default:
                break
            }
        }else{
            STUtils.displayAlert(message: "PLEASE_ENTER_LOCATION_SERVICE".localized, themeStyle: .error)
            return
        }
        locationManager.stopUpdatingLocation()
    }
}
