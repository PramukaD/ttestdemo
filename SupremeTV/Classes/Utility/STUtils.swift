//
//  STUtils.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/29/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit
import SwiftMessages

class STUtils: NSObject {

    class func displayAlert(message: String ,themeStyle: Theme?) {
        let aSwiftMessage = MessageView.viewFromNib(layout: .tabView)
        aSwiftMessage.configureTheme(themeStyle!)
        aSwiftMessage.configureContent(title: "", body: message)
        aSwiftMessage.button?.isHidden = true
        var aSwiftMessageConfig = SwiftMessages.defaultConfig
        aSwiftMessageConfig.presentationStyle = .top
        aSwiftMessageConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        aSwiftMessageConfig.duration = .seconds(seconds: 1.5)
        SwiftMessages.show(config: aSwiftMessageConfig, view: aSwiftMessage)
    }
    
    class func saveUser(user: User){
        if let data = try? JSONEncoder().encode(user) {
            UserDefaults.standard.set(data, forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_INFO)
        }
    }

    class func getUser() -> User{
        if let data = UserDefaults.standard.value(forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_INFO) as? Data,
            let user = try? JSONDecoder().decode(User.self, from: data) {
            return user
        }
        return User()
    }
    
    class func saveUserToken(token: String){
        UserDefaults.standard.setValue(token, forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_TOKEN)
    }
    
    class func getUserToken() -> String{
        return UserDefaults.standard.value(forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_TOKEN) as! String
    }

    class func saveLoginType(type: Double){
        UserDefaults.standard.setValue(type, forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_LOGIN_TYPE)
    }
    
    class func getLoginType() -> Double{
        return UserDefaults.standard.value(forKey: STConstant.STUserDefaultsKeys.SUPREMETV_USER_LOGIN_TYPE) as! Double
    }
    
    class func updateHeaders(){
        let token = STUtils.getUserToken()
        SwaggerClientAPI.customHeaders = ["Authorization" : "Bearer \(token)"]
    }
    
    class func userNavigation(user: User){
        STUtils.saveLoginType(type: user.loginType ?? 0)
        STUtils.saveUserToken(token: user.token ?? "")
        STUtils.saveUser(user: user)
        UserDefaults.standard.set(true, forKey: STConstant.STUserDefaultsKeys.IS_USER_LOGGEDIN)
        STUtils.updateHeaders()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.showHomeView()
    }
    
    class func getFormattedDate(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let aDate = dateFormatter.date(from: dateFormatter.string(from:date))
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: aDate!)
        return selectedDate
    }
    
    class func getPlayerUrl(tvShow: TvShowModel) -> String {
        if let watchNowUrl = tvShow.watchNowUrl {
            return watchNowUrl
        }else{
            return tvShow.url ?? ""
        }
    }
    
    class func isYoutubeUrl(youtubeUrl: String?) -> Bool {
        if youtubeUrl?.isEmpty ?? false || youtubeUrl == nil { return false }
        let id = URLComponents(string: youtubeUrl!)?.queryItems?.first(where: { $0.name == "v" })?.value
        if id?.isEmpty ?? false || id == nil { return false }else{ return true }
    }
    
    class func isUserLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: STConstant.STUserDefaultsKeys.IS_USER_LOGGEDIN)
    }
    
    class func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
    
    class func clearDirectory(){
        FileManager.default.clearTmpDirectory()
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("fileName.png")
        if fileManager.fileExists(atPath: paths){
            try! fileManager.removeItem(atPath: paths)
        }else{
            print("Something wronge.")
        }
    }
}
