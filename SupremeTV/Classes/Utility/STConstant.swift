//
//  STConstant.swift
//  SupremeTV
//
//  Created by Persystance Networks on 11/30/19.
//  Copyright © 2019 Persystance Networks. All rights reserved.
//

import UIKit

class STConstant: NSObject {

    struct STUserDefaultsKeys{
        static let IS_USER_LOGGEDIN          = "is_user_loggedin"
        static let SUPREMETV_USER_INFO          = "supremetv_user_info"
        static let SUPREMETV_USER_TOKEN          = "supremetv_user_token"
        static let SUPREMETV_USER_LOGIN_TYPE          = "supremetv_user_login_type"
        static let SIDE_MENU_SELECTED_INDEX          = "side_menu_selected_index"
        static let RELOAD_SIDE_MENU_NOTIFICATION         = "reload_side_menu_notification"
        static let RELOAD_HOME_NOTIFICATION         = "reload_home_notification"

        static let COMMON_ERROR_MESSAGE         = "COMMON_ERROR_MESSAGE".localized
        static let NO_INTERNET                = "NO_INTERNET".localized
        static let LOADING_TEXT                = "WAIT_MESSAGE".localized
    }
    
    struct STUrl{
        static let APP_LIVE_URL   = "youtube://www.youtube.com/embed/live_stream?channel=UCSsTFW6gPcjm63Fw86gZdSw"
        static let WEB_LIVE_URL   = "https://www.youtube.com/embed/live_stream?channel=UCSsTFW6gPcjm63Fw86gZdSw"

    }
}
